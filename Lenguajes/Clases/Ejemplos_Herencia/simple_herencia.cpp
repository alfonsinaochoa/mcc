#include <iostream>

using namespace std;

class Rectangulo
{
    protected:
       float longitud, altura;

    public:
        Rectangulo(): longitud(0.0), altura(0.0)
        {
            cout<<"Ingrese la longitud: ";
            cin>>longitud;

            cout<<"Ingrese la altura: ";
            cin>>altura;
        }

};

/* Clase Area derivada de la clase Rectangulo. */
class Area : public Rectangulo   
{
    public:
       float calcula()
         {
             return longitud*altura;
         }

};

/* Clase perimetro derivada de la clase Rectangulo. */
class Perimetro : public Rectangulo
{
    public:
       float calcula()
         {
             return 2*(longitud+altura);
         }
};

int main()
{
     
  cout<<"\nIngrese los datos del rectangulo para calcular area.\n\n";  
  Area a;
  cout<<"Area = "<<a.calcula()<<" metros cuadrados\n\n";

  cout<<"Ingrese los datos del rectangulo para calcular perimetro.\n\n";
  Perimetro p;
  cout<<"\nPerimetro = "<<p.calcula()<<" metros\n\n";

  return 0;
}
