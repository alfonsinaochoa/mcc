(ns linearregresion.core
(:gen-class)
(:use (incanter core stats charts io datasets pdf)
[clojure.core.matrix :only [transpose]]
;;[incanter.charts :only [scatter-plot xy-plot add-lines]]
;;[incanter.stats :only [linear-model quantile-normal]]
;;[incanter.datasets]
;;[incanter.core :only [view sel sqrt matrix to-matrix]]
)
(:require [clatrix.core :as cl]))


;;Funcion que lee el contenido de un archivo csv
(def read_csv
    (read-dataset "lluvia.csv" :header true)
)

(def X (cl/matrix (sel read_csv :cols 0)))
(def Y (cl/matrix (sel read_csv :cols 3)))

;;(def X (cl/matrix [8.401 14.475 13.396 12.127 5.044
;;                   8.339 15.692 17.108 9.253 12.029
;;                   22.112]))

;;(def Y (cl/matrix [-1.57 2.32 0.424 0.814 -2.3
;;                   0.01 1.954 2.296 -0.635 0.328 5.444]))


(defn linear-samp-scatter [X Y]
  (scatter-plot X Y))

(defn plot-scatter [X Y]
  (view (linear-samp-scatter X Y)))

(defn samp-linear-model [Y X]
  (linear-model Y X))

(:coefs samp-linear-model)
(:residuals samp-linear-model)
(:sse samp-linear-model)
(:r-square samp-linear-model)

(def coef_first (first (:coefs (samp-linear-model Y X))))
(def coef_second (second (:coefs (samp-linear-model Y X))))

;; y = w0 + w1(x)
(defn calculate [humedad]
	(println (+ coef_first (* coef_second (Float/parseFloat humedad)))))


(defn plot-model [X Y] (view
                      (add-lines (linear-samp-scatter X Y)
                                        X (:fitted (samp-linear-model Y X)))))

(defn sigmoid [z]
	(/ 1 (+ 1 (Math/exp (-z)))))



(defn -main []
(println "Leyendo archivo csv...")
;;(view read_csv)
(println "Ploteando datos...")
(plot-scatter X Y)
(samp-linear-model Y X)
(plot-model X Y)
;;(println "Error...")
;;(println (:r-square (samp-linear-model Y X)))
(println "Coeficientes...")
;;(println (:coefs (samp-linear-model Y X)))
(println coef_first)
(println coef_second)
(println "Ingrese el valor de humedad:")
(let[humedad (read-line)]
	(calculate humedad)
)
(System/exit 0)
)
