#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


void fun (int first, int second)
{
	first += first;
	second += second;
	printf("\nFirst: %d, Second: %d\n",first, second);
}

void func (int first, int second, int &primero, int &segundo)
{
	first += first;
	second += second;
	primero = first;
	segundo = second;
	
}

int main(int argc, char* argv[])
{
	int list[2] = {1, 3};
	int primero=0; int segundo=0;
	fun(list[0], list[1]);
	func(list[0], list[1], primero, segundo);	
	printf("\nFirst: %d, Second: %d\n",primero, segundo);
}



