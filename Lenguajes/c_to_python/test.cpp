#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
 
void swap(int a, int b, int *n)
{	
	a = b;
	b = *n;
	printf("a: %d; b: %d\n", a, b);
}

int main(int argc, char* argv[])
{
	int value = 2, list[5] = {1, 3, 5, 7, 9};
	printf("\nEn main-> a: %d; b: %d\n", value, list[0]);
	swap(value, list[0], &value);
	printf("\nEn main-> a: %d; b: %d\n", list[0], list[1]);
	swap(list[0], list[1], &list[0]);
	printf("\nEn main-> a: %d; b: %d\n", value, list[value]);
	swap(value, list[value], &value);
	return 0;
}

/*
void swap(int a, int b)
{
	int temp;
	temp = a;
	a = b;
	b = temp;
	printf("a: %d; b: %d\n", a, b);
}

int main(int argc, char* argv[])
{
	int value = 2, list[5] = {1, 3, 5, 7, 9};
	printf("\nEn main-> a: %d; b: %d\n", value, list[0]);
	swap(value, list[0]);
	printf("\nEn main-> a: %d; b: %d\n", list[0], list[1]);
	swap(list[0], list[1]);
	printf("\nEn main-> a: %d; b: %d\n", value, list[value]);
	swap(value, list[value]);
	return 0;
}
*/
