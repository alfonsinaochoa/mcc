(defproject icm "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
   :dependencies [[org.clojure/clojure "1.7.0"][semantic-csv "0.1.0"][incanter "1.5.6"][clojure-csv "2.0.1"][net.mikera/core.matrix "0.20.0"] [clatrix "0.3.0"]]
  :main ^:skip-aot icm.core
  :profiles {:dev {:plugins [[cider/cider-nrepl "0.9.1"]]}})
