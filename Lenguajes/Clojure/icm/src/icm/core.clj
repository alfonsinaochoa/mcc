(ns icm.core
  (:gen-class)
  (:use (incanter core stats charts io datasets pdf))
  (:require [clojure.java.io :as io]	   
            [clojure.string :as str]
            [clojure-csv.core :as csv])
)


(def lee_csv
    (read-dataset "indicadores_m.csv" :header true)
)


;;Funcion que retorna el calculo del indice de masa corporal
(defn imc [peso estatura]
  (let [total (/ (Float/parseFloat peso) (* (Float/parseFloat estatura)
                                          (Float/parseFloat estatura)))]
  (prn "Su indice de masa corporal es: " total)
  (cond
    (< total 16.00) "Delgadez severa"
    (and (< total 17.00)) "Delgadez moderada"
    (and (< total 18.50)) "Delgadez leve"
    (and (< total 25.00)) "Rango normal"
    (and (< total 30.00)) "Pre-obesidad"
    (and (< total 35.00)) "Obesidad clase I"
    (and (< total 40.00)) "Obesidad clase II"
    (and (> total 40.00)) "Obesidad clase III"
))
 )

;;Funcion que imprime el valor de las variables ingresadas
(defn print-message [peso estatura]
  (prn "Su peso es: " peso)
  (prn "Su estatura es: " estatura)
  (imc peso estatura)
)

;;Funcion que pide los datos del peso y estatura; y
;;valida que estos sean numeros y que sean mayores a 0
(defn get-data [mssg]
(loop []
(println mssg)
(def valor (read-line))
 (if-not(= "Salir" valor)
  (if (= valor "0")
    (do
     (println "El valor ingresado debe ser mayor a 0")
     (recur)
    )
      (if(re-seq #"^[0-9]+(\.[0-9]+)?$" valor)
        valor
        (do
           (println "El valor ingresado debe ser numerico")
           (recur)
         )
      )
    )
  "-1"
  )
 )
)


;;Funcion que devuelve el calculo del icm de acuerdo a los valores ingresados
(defn input[]
(let [peso (get-data  "Ingrese su peso(kg): ")]
  (if-not(= peso "-1")
    (let [estatura (get-data "Ingrese su estatura(mtrs):")]
      (if-not(= estatura "-1")
        (print-message peso estatura)       
      )
    )
  )
  (println "Saliendo del programa...\n")
)
)


;;Funcion que lee un archivo csv
(defn read_csv []  
  (with-open [lectura (io/reader "indicadores_m.csv")]
    (doseq [linea (line-seq lectura)]
      (println linea))))

;;Funcion que grafica los valores del csv (grafico de lineas)
(defn graph_line []
  (view (line-chart
            (sel lee_csv :cols 1)
            (sel lee_csv :cols 2)
 :x-label "Valores"
 :y-label "Paises"
 :title "Indicadores vs Paises"

 :group-by (sel lee_csv  :cols 0)
  :legend true
            )))

;;Funcion que grafica los valores del csv (grafico de barras)
(defn graph_bar[]
     (view (bar-chart
 (sel lee_csv :cols 1)
 (sel lee_csv :cols 2)
 :x-label "Valores"
 :y-label "Paises"
 :title "Indicadores vs Paises"
                    :group-by (sel lee_csv :cols 0)
                    :legend true)))

(defn -main []
(println "Calculo del indice de masa corporal")
(input)
(println "Leyendo archivo ...")
(read_csv)
(prn "La dimension de la tabla de Indicadores es: " (dim lee_csv))
(println "Graficando ...")
(graph_line)
(graph_bar)
)
