(ns read-file.core
  (:gen-class)
  (:require [clojure.java.io :as io] [clojure-csv.core :as csv]))

(defn lee_csv
  "Recive el nombre de archivo y lee los datos."
  [fname]
  (with-open [lectura (io/reader fname)]
    (doseq [linea (line-seq lectura)]
      (println linea))))


(defn -main
  "Programa recibe desde consola el nombre del archivo"
  [nomarch]
  (println "Leyendo archivo...")  
  (lee_csv nomarch))
