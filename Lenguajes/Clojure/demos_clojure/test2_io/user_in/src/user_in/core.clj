(ns user-in.core
  (:gen-class))

(defn -main
  "Esta es la descripción de la función."
  [& args]
  (println "Ingrese el texto:")
  (loop [input (read-line)]
    (when-not (= ":listo" input)
      (println (str "Ingresaste: -> " input " <-"))
      (recur (read-line))))
  )
