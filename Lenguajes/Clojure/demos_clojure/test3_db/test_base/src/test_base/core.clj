(ns test-base.core
(:require [clojure.java.jdbc :as sql])
  (:gen-class))

(def mysql-db {:subprotocol "mysql"
               :subname "//127.0.0.1:3306/clojure_test"
               :user "clojure_test"
               :password "clojure_test"})


(defn -main
  "Esta es la descripcion de la funcion."
  [& args]
  (sql/insert! mysql-db :fruta
               {:nombre "Manzana" :apariencia "verde" :costo 2}
               {:nombre "Naranja" :apariencia "redonda" :costo 1})

  (println (sql/query mysql-db
             ["select * from fruta where apariencia = ?" "verde"]
             :row-fn :nombre))
  )
