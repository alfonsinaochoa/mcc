create database clojure_test;
USE clojure_test;

CREATE TABLE fruta (
    id int(11) NOT NULL AUTO_INCREMENT,
    nombre varchar(12),
    apariencia varchar(12),
    costo int(11),
    PRIMARY KEY(id)
    );

CREATE USER 'clojure_test'@'localhost' IDENTIFIED BY 'clojure_test';
GRANT ALL PRIVILEGES ON *.* TO 'clojure_test'@'localhost' WITH GRANT OPTION;