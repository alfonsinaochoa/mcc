(prn "Hello World")
(println "Hello World")
(pr-str "Hello World")

;; Espacios de nombres (namespaces)
(ns wonderland
  (:require [clojure.set :as adv]))

;; Información sobre el namespace
*ns*

;; Cambiar de namespace: (ns nombre_ns)

;; Uso de listas
'(1 2 "hojas" :plantas)

(first '(:hojas :tallo :frutos :raiz))
(rest '(:hojas :tallo :frutos :raiz))

(first (rest '(:hojas :tallo :frutos :raiz)))

;; Llenando listas (2 args)
(cons 5 '())
(cons 5 nil)

(cons 4 (cons 5 nil))

'(1 2 3 4 5)

(list 1 2 3 4 5)

;; Usando vectores

[:hojas 1 2 3 :tallo]

(first [:hojas 1 2 3 :tallo])

(rest [:hojas 1 2 3 :tallo])

(nth [:hojas 1 2 3 :tallo] 1)

(last [:hojas 1 2 3 :tallo])

(count [:hojas 1 2 3 :tallo])

;; Con vectores, conj agrega al final
(conj [:tallo :hojas] :frutos)
(conj [:tallo :hojas] :frutos :semillas)


;; Con listas, conj agrega al inicio

(conj '(:tallo :hojas) :raiz)

;; Uso de mapas (clave-valor)

{:raiz "tierra" :tallo "aire"}

{:raiz "tierra", :tallo "aire", :frutos "semillas"}

(get {:raiz "tierra", :tallo "aire", :frutos "semillas"} :tallo)

(:tallo {:raiz "tierra", :tallo "aire", :frutos "semillas"})

(keys {:raiz "tierra", :tallo "aire", :frutos "semillas"})

(vals {:raiz "tierra", :tallo "aire", :frutos "semillas"})

(assoc {:raiz "tierra", :tallo "aire", :frutos "semillas"} :hojas "fotosintesis")

(dissoc {:raiz "tierra", :tallo "aire", :frutos "semillas"} :hojas)

(merge {:raiz "tierra", :tallo "aire"} {:frutos "semillas", :hojas "fotosintesis"})

;; Operaciones de Clojure Set

(adv/union #{:r :b :w} #{:w :p :y})
(adv/difference #{:r :b :w} #{:w :p :y})
(adv/intersection #{:r :b :w} #{:w :p :y})
;; ...

;; Definición de Funciones
(defn area_rect [base altura]
(/ (* base altura) 2))

(defn llena_map [nombre edad]
  {:texto "Persona" :nom nombre :age edad})


(defn common-fav-foods [foods1 foods2]
  (let [food-set1 (set foods1)
        food-set2 (set foods2)
        common-foods (adv/intersection food-set1 food-set2)]
    (str "Common Foods: " common-foods)))

(common-fav-foods [:jam :brownies :toast]
                  [:lettuce :carrots :jam])

(defn drinkable [x]
  (= x :drinkme))


;;Condicionales
(if true "es verdad" "no es verdad")

(if (= :drinkme :drinkme)
  "try it"
  "Don't try it")

(let [need-to-grow-small (> 5 3)]
     (if need-to-grow-small
       "drink bottle"
       "don't drink bottle"))

(if-let [need-to-grow-small (> 5 3)]
      "drink bottle"
       "don't drink bottle")

(defn drink [need-to-grow-small]
  (when need-to-grow-small "drink bottle"))

(when-let [need-to-grow-small true]
  "drink bottle")

(let [bottle "mystery"]
  (cond
    (= bottle "poison") "don't touch"
    (= bottle "drinkme") "grow smaller"
    (= bottle "empty") "all gone"
    :else "unknown"))

(let [bottle "mystery"]
  (case bottle
    "poison" "don't touch"
    "drinkme" "grow smaller"
    "empty" "all gone"
    "unknown"))

(defn findout [bottle]
  (cond
    (= bottle "poison") "don't touch"
    (= bottle "drinkme") "grow smaller"
    (= bottle "empty") "all gone"
    :else "unknown"))

(defn grow [name direction]
  (if (= direction :small)
    (str name " is growning smaller")
    (str name " is growning bigger")))

(grow "Alice" :small)
(grow "Alice" :big)


;; Paso parcial de argumentos
(partial grow "Alice")
((partial grow "Alice") :small)

(defn toggle-grow [direction]
(if (= direction :small) :big :small))

(toggle-grow :big)

(defn oh-my [direction]
  (print "Oh My! You are growing %1 %2" direction))

(def adjs ["normal"
           "too small"
           "too big"
           "is swimming"])

(defn alice-is [in out]
  (if (empty? in)
    out
    (alice-is
     (rest in)
     (conj out
           (str "Alice is " (first in))))))

(alice-is adjs [])

(defn alice-is [input]
  (loop [in input
         out []]
    (if (empty? in)
      out
      (recur (rest in)
             (conj out
                   (str "Alice is " (first in)))))))

(alice-is adjs)


(defn countdown [n]
  (if (= n 0)
    n
    ((println n)
    (countdown (- n 1)))))

(countdown 3)
(countdown 100000)

(defn countdown [n]
  (if (= n 0)
    n
    (recur (- n 1))))

(countdown 3)
(countdown 100000)


(defn surprise [direction]
  ((comp oh-my toggle-grow) direction))

(surprise :small)

;;*****************************

(defn adder [x y]
  (+ x y))

(adder 3 4)

(def adder-5 (partial adder 5))

(adder-5 10)


(count (take 1000 (range)))

(take 5 (repear "Rabbit")

(defn indice-masa-corporal [altura peso]
(/ peso (* altura altura)))

(defn param[]
(println "Ingrese su peso(kg): ")
(let [peso (read-line)]
  println(read-line)
  (if (= peso 0)
    (println "El peso debe ser mayor a 0"))
(println "Ingrese su altura(cm): ")
(let [altura (read-line)]
  (if (= altura 0)
    (println "La altura debe ser mayor a 0")))))

(defn print-message [pid pw]
  (println "The height must be higher than 0. Please try again!" pid)
  (println "Your weight is :" pw))

(defn funcion[]
(println "Enter your height and weight")
(let[height (read-line) weight (read-line)]
(if (= height "0") (println "The height must be higher than 0. Please try again!"))
(indice-masa-corporal height weight)))

;;funcion que pide la altura y el peso
(defn function[]
(println "Enter your height and weight")
(let[height (read-line) weight (read-line)]
(if (= height "0") (println "The height must be higher than 0. Please try again!"))
(indice-masa-corporal (Float/parseFloat height) (Float/parseFloat weight))))


;;funcion que calcula el indice de masa corporal
(defn indice-masa-corporal [altura peso]
(let[t (/  peso (* altura altura))]
 (cond
    (> t 40.00) "Obesidad morbida"
    (> t 35.00) "Obesidad media"
    (> t 30.00) "Obesidad leve"
    :else "Normal")
))

(defn inp2[]
(println "Enter your height and weight")
(let[height (read-line) weight (read-line)]
(if (= (Float/parseFloat(height)) 0) (println "The height must be higher than 0. Please try again!"))
(print-message height weight) ))


(defn inp[]
(println "Enter your height and weight")
(let[height (read-line) weight (read-line)]
(if (= height "0") (println "The height must be higher than 0. Please try again!"))
(print-message height weight)))

(let [yayinput (read-line)]
  (if (=yayinput "1234")
    (println "Correct")
    (println "Wrong")))



