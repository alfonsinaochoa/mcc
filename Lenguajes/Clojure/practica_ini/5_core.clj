;; Encabezado y llamada a librerias
(ns intro.core
  "Operaciones basicas con matrices"
  (:use clojure.core.matrix)
  (:require [clojure.core.matrix.operators :as M]
            [clatrix.core :as cl]))


;; Definición de matriz usando clatrix
(def A (cl/matrix [[0 1 2] [3 4 5]]))
;;A
;;(pm A)

(cl/get A 1 0)

(cl/get A 5)

(row-count (cl/matrix [0 1 2]))
(column-count (cl/matrix [0 1 2]))

;;(cl/set A 1 2 0)
