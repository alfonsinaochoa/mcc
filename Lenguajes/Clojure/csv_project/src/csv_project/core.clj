(ns csv-project.core
(:require
[clojure.string :as str]
[schema.utils :as utils]))


(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println (parse-csv "1,2,3,hello,world")))

(defn imc [peso estatura]
  (let [total (/ (Float/parseFloat peso) (* (Float/parseFloat estatura)
                                          (Float/parseFloat estatura)))]
  (prn "Su indice de masa corporal es: " total)
  (cond
    (< total 16.00) "Delgadez severa"
    (and (> total 16.00) (< total 16.99)) "Delgadez moderada"
    (and (> total 17.00) (< total 18.49)) "Delgadez leve"
    (and (> total 18.50) (< total 24.99)) "Rango normal"
    (and (> total 25.00) (< total 29.99)) "Pre-obesidad"
    (and (> total 30.00) (< total 34.99)) "Obesidad clase I"
    (and (> total 35.00) (< total 39.99)) "Obesidad clase II"
    (and (> total 40.00)) "Obesidad clase III"
))
 )


(defn print-message [peso estatura]
  (prn "Su peso es: " peso)
  (prn "Su estatura es: " estatura)
  (imc peso estatura)
)

(defn inp []
  (println "Ingrese su peso(kg) y la estatura(mtrs): ")
  (let [peso (read-line) estatura (read-line)]
    (when-not (or (= "Salir" peso) (= "Salir" estatura))
      (if-not(or (re-seq #"^[0-9]+(\.[0-9]+)?$" estatura) (re-seq #"^[0-9]+(\.[0-9]+)?$" peso))
        ;;((println "debe ser mayor a cero")
        (recur)
        (print-message peso estatura)))
    )
)
;;(boolean (validate-value-zero "0"))

(defn get-data [mssg]
(loop []
(println mssg)
(def valor (read-line))
(if-not(= "Salir" valor)
  (if (= valor "0")
    (do
     (println "El valor ingresado debe ser mayor a 0")
     (recur)
    )
      (if(re-seq #"^[0-9]+(\.[0-9]+)?$" valor)
        valor
        (do
           (println "El valor ingresado debe ser numerico")
           (recur)
         )
      )
      )
  -1
  )
 )
)

(defn input[]
(let [= result (get-data  "Ingrese su peso(kg): ")]
  println ("hola mundo")
)
;;(println "Ingrese su altura(kg): ")
)


(defn inp []
(loop []
(println "Ingrese su peso (kg): ")
(def peso (read-line))
(when-not(= "Salir" peso)
  (if-not(boolean (validate-value-zero peso))
    (recur)))
)
(when-not(= "Salir" peso)
(loop []
(println "Ingrese su estatura(mtrs): ")
(def estatura (read-line))
(when-not(= "Salir" estatura)
  (if-not(boolean (validate-value-zero estatura))
    (recur)))
)
)
(when-not(or (= "Salir" peso) (= "Salir" estatura))
(print-message peso estatura)
)
)

(defn validate-value-zero [valor]
(if (= valor "0")
    (println "El valor ingresado debe ser mayor a 0")
    true
))


(defn param[]
(println "Ingrese su peso(kg): ")
(def peso (read-line))
(when-not (= "Salir" peso)
(if (= peso "0")
    (println "El valor del peso ingresado debe ser mayor a 0") 
(if-not(re-seq #"^[0-9]+(\.[0-9]+)?$" peso) 
(println "El valor del peso debe ser un numero")))
(println "Ingrese su estatura(mtrs): ")
(def estatura (read-line))
(when-not (= "Salir" estatura)
(if (= estatura "0")
    (println "El valor de la estatura ingresada debe ser mayor a 0") 
(if-not(re-seq #"^[0-9]+(\.[0-9]+)?$" estatura) 
(println "El valor de la estatura debe ser un numero")
(print-message peso estatura)
))))
)



(defn param[]
(println "Ingrese su peso(kg): ")
(let [peso (read-line)] 
  (if (= peso "0")
    (println "El peso debe ser mayor a 0")
((println "Ingrese su estatura(mtrs): ")
(let [altura (read-line)]
  (if (= altura "0")
    (println "La altura debe ser mayor a 0")
))))))

;;funcion que solicita dos parametros de entrada
(defn input-string[]
(println "Ingrese su peso(kg): ")
(def peso (read-line))
(println "Ingrese su altura(kg): ")
(def altura (read-line))
)


;;funcion que calcula el indice de masa corporal
(defn indice-masa-corporal [altura peso]
(let[t (/  peso (* altura altura))]
 (cond
    (> t 40.00) "Obesidad morbida"
    (> t 35.00) "Obesidad media"
    (> t 30.00) "Obesidad leve"
    :else "Normal")
))


;;funcion que pide la altura y el peso
;;(defn function[]
;;(println "Enter your height and weight")
;;(let[height (read-line) weight (read-line)]
;;(if (= height "0") (println "The height must be higher than 0. Please try again!"))
;;(indice-masa-corporal (Float/parseFloat height) (Float/parseFloat weight))))

;;(defn validatestring[]
;;(println "Enter your height and weight")
;;(let[height (read-line)]
;;(if (not(string? height)) (println "The height must be higher than 0. Please try again!")
;;(println "Todo bien" ))))

;(re-seq #"[0-9]+" "abs123def345ghi567")


;;funcion que calcula el indice de masa corporal
;;(defn indice-masa-corporal [altura peso]
;;(let[t (/  peso (* altura altura))]
;; (cond
;;    (> t 40.00) "Obesidad morbida"
;;    (> t 35.00) "Obesidad media"
;;    (> t 30.00) "Obesidad leve"
;;    :else "Normal")
;;))

