(ns icm.core
  (:gen-class)
  (:use (incanter core stats charts io datasets pdf))
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure-csv.core :as csv])
)


(def data
  (read-dataset
    "cars.csv"
     :header true))

(def va-matrix
(to-matrix ($ [:Indicador] data)))

(def datasetcsv (get-dataset :cars))

;;Funcion que retorna el calculo del indice de masa corporal
(defn imc [peso estatura]
  (let [total (/ (Float/parseFloat peso) (* (Float/parseFloat estatura)
                                          (Float/parseFloat estatura)))]
  (prn "Su indice de masa corporal es: " total)
  (cond
    (< total 16.00) "Delgadez severa"
    (and (> total 16.00) (< total 16.99)) "Delgadez moderada"
    (and (> total 17.00) (< total 18.49)) "Delgadez leve"
    (and (> total 18.50) (< total 24.99)) "Rango normal"
    (and (> total 25.00) (< total 29.99)) "Pre-obesidad"
    (and (> total 30.00) (< total 34.99)) "Obesidad clase I"
    (and (> total 35.00) (< total 39.99)) "Obesidad clase II"
    (and (> total 40.00)) "Obesidad clase III"
))
 )

;;Funcion que imprime el valor de las variables ingresadas
(defn print-message [peso estatura]
  (prn "Su peso es: " peso)
  (prn "Su estatura es: " estatura)
  (imc peso estatura)
)

(defn lee_csv []  
  (with-open [lectura (io/reader "Indicadores.csv")]
    (doseq [linea (line-seq lectura)]
      (println str/lower-case linea))))

(defn map-data [dataset column fn]
  (conj-cols (sel dataset :except-cols column)
             ($map fn column dataset)))

;;(def data (get-dataset :cars))
;;(map-data data :speed #(* % 2))

(defn update-column
         [dataset column f & args]
         (->> (map #(apply update-in % [column] f args) (:rows dataset))
           vec
           (assoc dataset :rows)))


(defn -main []
(println "Leyendo archivo ...")
(println (update-column datasetcsv :col-1 str "d"))
;;(view (histogram (sample-normal 1000)))
(lee_csv)
(println (dim data))
(println (col-names data))
;;(println (
;;count
;;va-matrix))
;;(println (
;;first
;;va-matrix))
;;(with-data data;;
;;  (view $data)
  ;;(def lm (linear-model ($ :dist) ($ :speed)))
  ;;(doto (scatter-plot ($ :speed) ($ :dist))
    ;;(add-lines ($ :speed) (:fitted lm))
   ;; view)
;;)
;;(view (fetch-dataset :cars))
;;(with-data data
;;[(println (mean ($ :Chile)))
;;(println (sd ($ :Chile)))])
)
