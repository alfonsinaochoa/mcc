(ns clj-ml1.matrix-basics
  "Basic matrix operation"
  (:use clojure.core.matrix)
  (:require [clojure.core.matrix.operators :as M]
            [clatrix.core :as cl]))

(defn mat-eq
  "Checks if two matrices are equal"
  [A B]
  (and (= (count A) (count B))
       (reduce #(and %1 %2) (map = A B))))

;; can also use this in ns form
;; to avoid confilcts with fns in clojure namespace
;;
;; (:refer-clojure :exclude [+ - *])

(defn square-mat
  "Creates a square matrix of size n x n whose elements are all e"
  [n e]
  (let [repeater #(repeat n %)]
    (matrix (-> e repeater repeater))))

(defn square-mat
  "Creates a square matrix of size n x n whose elements are all e.
   Accepts an option argument for the matrix implementation."
  [n e & {:keys [implementation] :or {implementation :persistent-vector}}]
  (let [repeater #(repeat n %)]
    (matrix implementation (-> e repeater repeater))))

;; user> (square-mat 2 1)
;; [[1 1] [1 1]]
;; user> (square-mat 2 1 :implementation :clatrix)
;;  A 2x2 matrix
;;  -------------
;;  1.00e+00  1.00e+00
;;  1.00e+00  1.00e+00


;; identity matrix

(defn id-mat
  "Creates an identity matrix of n x n size"
  [n]
  (let [init (square-mat :clatrix n 0)
        identity-f (fn [i j n]
                     (if (= i j) 1 n))]
    (cl/map-indexed identity-f init)))

;; user> (id-mat 5)
;;  A 5x5 matrix
;;  -------------
;;  1.00e+00  0.00e+00  0.00e+00  0.00e+00  0.00e+00
;;  0.00e+00  1.00e+00  0.00e+00  0.00e+00  0.00e+00
;;  0.00e+00  0.00e+00  1.00e+00  0.00e+00  0.00e+00
;;  0.00e+00  0.00e+00  0.00e+00  1.00e+00  0.00e+00
;;  0.00e+00  0.00e+00  0.00e+00  0.00e+00  1.00e+00
;; user> (pm (identity-matrix 5))
;; [[1.000 0.000 0.000 0.000 0.000]
;;  [0.000 1.000 0.000 0.000 0.000]
;;  [0.000 0.000 1.000 0.000 0.000]
;;  [0.000 0.000 0.000 1.000 0.000]
;;  [0.000 0.000 0.000 0.000 1.000]]
;; nil

;; this was first wrongly implemented as follows
;;
;; (defn rand-square-mat [n]
;;   (matrix (repeat n (repeat n (rand-int 100))))) ;; this won't work
;;
;; user> (rand-square-mat 2)
;; [[94 94] [94 94]]


;; user> (rand-square-mat 2)
;; [[65 49] [85 78]]

(defn rand-square-clmat
  "Generates a random clatrix matrix of size n x n"
  [n]
  (cl/map rand-int (square-mat n 100 :implementation :clatrix)))

(defn rand-square-mat
  "Generates a random matrix of size n x n"
  [n]
  (matrix
   (repeatedly n #(map rand-int (repeat n 100)))))

;; user> (pm (rand-square-mat 4))
;; [[97.000 35.000 69.000 69.000]
;;  [50.000 93.000 26.000  4.000]
;;  [27.000 14.000 69.000 30.000]
;;  [68.000 73.000 0.0007 3.000]]
;; nil
;; user> (rand-square-clmat 4)
;;  A 4x4 matrix
;;  -------------
;;  5.30e+01  5.00e+00  3.00e+00  6.40e+01
;;  6.20e+01  1.10e+01  4.10e+01  4.20e+01
;;  4.30e+01  1.00e+00  3.80e+01  4.70e+01
;;  3.00e+00  8.10e+01  1.00e+01  2.00e+01
;; nil

;; note that (matrix [0 1]) is a 2x1 matrix
;; and       (matrix [[0 1]]) is a 1x2 matrix

;; generate a matrix of random elements
;; by specifying the mean and standard deviation
(def norm-mat (cl/rnorm 10 25 10 10))
;; print the matrix using (pm norm-mat)

(def map-norm-mat (cl/map #(* 100 %) norm-mat))
;; we can also use map-indexed, which expects a fn #(i j a)
;; to treat a matrix a vector of vectors, use as-vec or dense functions
;; as-vec will flatten matrices with only 1 row into a single vector

;; user> (cl/rnorm 10 25 10 10)
;;  A 10x10 matrix
;;  ---------------
;; -1.25e-01  5.02e+01 -5.20e+01  .  5.07e+01  2.92e+01  2.18e+01
;; -2.13e+01  3.13e+01 -2.05e+01  . -8.84e+00  2.58e+01  8.61e+00
;;  4.32e+01  3.35e+00  2.78e+01  . -8.48e+00  4.18e+01  3.94e+01
;;  ...
;;  3.58e+01  2.70e-01 -1.81e+01  .  4.55e+01  2.41e+01 -1.75e+00
;;  1.55e+01  1.24e+01  2.20e+01  . -8.99e+00  4.86e+01 -1.79e+01
;;  1.43e+01 -6.74e+00  2.62e+01  . -2.06e+01  8.14e+00 -2.69e+01
;; user> (cl/rnorm 5)
;;  A 5x1 matrix
;;  -------------
;;  1.18e+00
;;  3.46e-01
;; -1.32e-01
;;  3.13e-01
;; -8.26e-02
;; user> (cl/rnorm 3 4)
;;  A 3x4 matrix
;;  -------------
;; -4.61e-01 -1.81e+00 -6.68e-01  7.46e-01
;;  1.87e+00 -7.76e-01 -1.33e+00  5.85e-01
;;  1.06e+00 -3.54e-01  3.73e-01 -2.72e-02

(defn id-computed-mat
  "Creates an identity matrix of size n x n using compute-matrix"
  [n]
  (compute-matrix [n n] #(if (= %1 %2) 1 0)))

(defn rand-computed-mat
  "Creates an n x m matrix of random elements using compute-matrix"
  [n m]
  (compute-matrix [n m]
                  (fn [i j] (rand-int 100))))


;; cl/get
;; user> (def A (cl/matrix [[0 1 2] [3 4 5]]))
;; #'user/A
;; user> (cl/get A 1 1)
;; 4.0
;; user> (cl/get A 3)
;; 4.0

;; user> (row-count (cl/matrix [0 1 2]))
;; 3
;; user> (column-count (cl/matrix [0 1 2]))
;; 1

;; cl/set
;; user> (pm A)
;; [[0.000 1.0002.000]
;;  [3.000 4.0005.000]]
;; nil
;; user> (cl/set A 1 2 0)
;; #<DoubleMatrix [0.000000, 1.000000, … , 0.000000]>
;; user> (pm A)
;; [[0.000 1.000 2.000]
;;  [3.000 4.000 0.000]]
;; nil

;; define 3 matrices for multiplication
(def A (matrix [[1 2 3] [4 5 6]]))

(def B (matrix [[10 20] [20 30] [30 40]]))

(def C (matrix [[11 12] [13 14]]))

;; matrix-matrix multiplication
;;
;; user> (pm (M/* A B))
;; [[140.000 200.000]
;;  [320.000 470.000]]
;; nil
;; user> (pm (M/* A C))
;; RuntimeException Mismatched vector sizes  clojure.core.matrix.impl.persistent-vector/eval4794/fn--4805 (persistent_vector.clj:212)
;; user> (pm (M/* A N))
;; CompilerException java.lang.RuntimeException: Unable to resolve symbol: N in this context, compiling:(/tmp/form-init161494247667328494.clj:1:5)
;; user> (def N 10)
;; #'user/N
;; user> (pm (M/* A N))
;; [[10.000 20.000 30.000]
;;  [40.000 50.000 60.000]]
;; nil

;; scale
;;
;; user> (pm (scale A 10))
;; [[10.000 20.000 30.000]
;;  [40.000 50.000 60.000]]
;; nil
;; user> (M/== (scale A 10) (M/* A 10))
;; true


;; (defn time-mat [n]
;;   (let [A (rand-square-mat n)
;;         B (rand-square-mat n)]
;;     (time (M/* A B))))

;; (defn time-clmat [n]
;;   (let [A (rand-square-clmat n)
;;         B (rand-square-clmat n)]
;;     (time (M/* A B))))

(defn time-mat-mul
  "Measures the time for multiplication of two matrices A and B"
  [A B]
  (time (M/* A B)))

(defn core-matrix-mul-time []
  (let [A (rand-square-mat 100)
        B (rand-square-mat 100)]
    (time-mat-mul A B)))

(defn clatrix-mul-time []
  (let [A (rand-square-clmat 100)
        B (rand-square-clmat 100)]
    (time-mat-mul A B)))

;; core-matrix-mul-time takes around 1 sec on average
;; clatrix-mul-time takes around 40 ms the first time,
;; and less than 1 ms after that

;; basic matrix equality
(defn mat-eq
  "Checks if two matrices are equal"
  [A B]
  (reduce #(and %1 %2) (map = A B)))

(defn mat-add
  "Add two matrices"
  [A B]
  (mapv #(mapv + %1 %2) A B))

(defn mat-add
  "Add two or more matrices"
  ([A B]
     (mapv #(mapv + %1 %2) A B))
  ([A B & more]
     (let [M (concat [A B] more)]
       (reduce mat-add M))))

;; transpose of a matrix
;;
;; user> (def A (matrix [[1 2 3] [4 5 6]]))
;; #'user/A
;; user> (pm (transpose A))
;; [[1.000 4.000]
;;  [2.000 5.000]
;;  [3.000 6.000]]
;; nil
;;

;; determinant of a matrix
;;
;; user> (def A (cl/matrix [[-2 2 3] [-1 1 3] [2 0 -1]]))
;; #'user/A
;; user> (det A )
;; 6.0

;; inverse of a matrix
;;
;; user> (def A (cl/matrix [[2 0] [0 2]]))
;; #'user/A
;; user> (M/* (inverse A) A)
;;  A 2x2 matrix
;;  -------------
;;  1.00e+00  0.00e+00
;;  0.00e+00  1.00e+00
;;
;; user> (def A (cl/matrix [[1 2] [3 4]]))
;; #'user/A
;; user> (inverse A)
;;  A 2x2 matrix
;;  -------------
;; -2.00e+00  1.00e+00
;;  1.50e+00 -5.00e-01
;;
;; user> (def A (cl/matrix [[1 2 3] [4 5 6]]))
;; #'user/A
;; user> (inverse A)
;; ExceptionInfo throw+: {:exception "Cannot invert a non-square matrix."}  clatrix.core/i (core.clj:1033)
;;
;; user> (def A (cl/matrix [[2 0] [2 0]]))
;; #'user/A
;; user> (M/* (inverse A) A)
;; LapackException LAPACK DGESV: Linear equation cannot be solved because the matrix was singular.  org.jblas.SimpleBlas.gesv (SimpleBlas.java:274)
;;
