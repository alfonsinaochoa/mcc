(ns ^{:author "Jonas Enlund"
:doc "Reading and writing comma separated values."}
clojure.data.csv
(:require (clojure [string :as str]))
(:import (java.io PushbackReader Reader Writer StringReader EOFException)))

(def ^{:private true} lf (int \newline))
(def ^{:private true} cr (int \return))
(def ^{:private true} eof -1)

