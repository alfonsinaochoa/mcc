; CIDER 0.9.1 (Java 1.7.0_85, Clojure 1.7.0, nREPL 0.2.10)
WARNING: The following required nREPL ops are not supported: 
apropos classpath complete eldoc format-code format-edn info inspect-pop inspect-push inspect-refresh macroexpand ns-list ns-vars ns-path refresh resource stacktrace toggle-trace-var toggle-trace-ns undef
Please, install (or update) cider-nrepl 0.9.1 and restart CIDER
WARNING: CIDER's version (0.9.1) does not match cider-nrepl's version (not installed)
user> (prn "Hello Wolrd")
"Hello Wolrd"
nil
user> (ns wonderland
  (:require [clojure.set :as adv]))
nil
wonderland> *ns*
#object[clojure.lang.Namespace 0x461384f2 "wonderland"]
wonderland> (ns practica.core)
nil
practica.core> (ns wonderland.core)
nil
wonderland.core> (ns user.core)
nil
user.core> '(1 2 "hojas" :plantas)
(1 2 "hojas" :plantas)
user.core> '(1 2 "hojas" :plantas)
(1 2 "hojas" :plantas)
user.core> '(1 2 "hojas" :plantas)
(1 2 "hojas" :plantas)
user.core> (ns user.core)
nil
user.core> '(1 2 "hojas" :plantas)

(1 2 "hojas" :plantas)
user.core> '(1 2 "hojas" :plantas)

(1 2 "hojas" :plantas)
user.core> (first '(:hojas :tallo :frutos :raiz))
:hojas
user.core> (rest '(:hojas :tallo :frutos :raiz))
(:tallo :frutos :raiz)
user.core> (first (rest '(:hojas :tallo :frutos :raiz)))
:tallo
user.core> (cons 5 '())
(5)
user.core> (cons 4 (cons 5 nil))

(4 5)
user.core> [:hojas 1 2 3 :tallo]

[:hojas 1 2 3 :tallo]
user.core> (nth [:hojas 1 2 3 :tallo] 1)

1
user.core> (nth [:hojas 1 2 3 :tallo] 2)

2
user.core> (conj [:tallo :hojas] :frutos)
[:tallo :hojas :frutos]
user.core> (conj [:tallo :hojas] :frutos :semillas)

[:tallo :hojas :frutos :semillas]
user.core> {:raiz "tierra", :tallo "aire", :frutos "semillas"}

{:raiz "tierra", :tallo "aire", :frutos "semillas"}
user.core> (get {:raiz "tierra", :tallo "aire", :frutos "semillas"} :tallo)

"aire"
user.core> (:tallo {:raiz "tierra", :tallo "aire", :frutos "semillas"})

"aire"
user.core> (keys {:raiz "tierra", :tallo "aire", :frutos "semillas"})

(:raiz :tallo :frutos)
user.core> (vals {:raiz "tierra", :tallo "aire", :frutos "semillas"})

("tierra" "aire" "semillas")
user.core> (assoc {:raiz "tierra", :tallo "aire", :frutos "semillas"} :hojas "fotosintesis")

{:raiz "tierra", :tallo "aire", :frutos "semillas", :hojas "fotosintesis"}
user.core> (dissoc {:raiz "tierra", :tallo "aire", :frutos "semillas"} :hojas)

{:raiz "tierra", :tallo "aire", :frutos "semillas"}
user.core> (dissoc {:raiz "tierra", :tallo "aire", :frutos "semillas"} :raiz)

{:tallo "aire", :frutos "semillas"}
user.core> (adv/union #{:r :b :w} #{:w :p :y})
CompilerException java.lang.RuntimeException: No such namespace: adv, compiling:(/tmp/form-init6409642179889800235.clj:1:1) 
user.core> (defn area_rect [base altura]
(/ (* base altura) 2))
#'user.core/area_rect
user.core> (area_rect 4 2)
4
user.core> (ns wonderland
  (:require [clojure.set :as adv]))
nil
wonderland> (adv/union #{:r :b :w} #{:w :p :y})
#{:y :r :w :b :p}
wonderland> (ns user)
nil
user> (ns user.core)
nil
user.core> (defn llena_map [nombre edad]
  {:texto "Persona" :nom nombre :age edad})
#'user.core/llena_map
user.core> (llena_map "Ingrid" 29)
{:texto "Persona", :nom "Ingrid", :age 29}
user.core> 
(defn common-fav-foods [foods1 foods2]
  (let [food-set1 (set foods1)
        food-set2 (set foods2)
        common-foods (adv/intersection food-set1 food-set2)]
    (str "Common Foods: " common-foods)))

CompilerException java.lang.RuntimeException: No such namespace: adv, compiling:(/tmp/form-init6409642179889800235.clj:5:22) 
user.core> (ns wonderland)
nil
wonderland> 
(defn common-fav-foods [foods1 foods2]
  (let [food-set1 (set foods1)
        food-set2 (set foods2)
        common-foods (adv/intersection food-set1 food-set2)]
    (str "Common Foods: " common-foods)))

#'wonderland/common-fav-foods
wonderland> 
(common-fav-foods [:jam :brownies :toast]
                  [:lettuce :carrots :jam])

"Common Foods: #{:jam}"
wonderland> 
(defn drinkable [x]
  (= x :drinkme))

#'wonderland/drinkable
wonderland> (drinkable 4)
false
wonderland> (drinkable 10)
false
wonderland> (drinkable :drinkme)
true
wonderland> (if
 true "es verdad" "no es verdad")

"es verdad"
wonderland> (if fa

lse "es verdad" "no es verdad")

"no es verdad"
wonderland> (defn llena_map [nombre edad]
  {:texto "Persona" :nom nombre :age edad})



