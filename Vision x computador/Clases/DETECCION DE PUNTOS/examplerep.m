

f = dir('Test_Images/1/*');
tasaRepetibilidad = zeros(length(f) -3, 11);
tasaRepetibilidad(:,1) = 1;
rotAngles = 0:15:360;
X = java_array('java.lang.String', length(f) -3);

h = 2;

imageFiles = {'Cover-01.jpg', 'Cover-02.jpg'};
numFeatureMatches = zeros(length(f) -h, length(rotAngles));
for x = h+1:length(f)
    fname =f(x).name
    
    X(x-h) = java.lang.String(fname);
    
    imgOrig=rgb2gray( imread(fullfile('Test_Images/1/',f(x).name)));
    
    [height, width] = size(imgOrig);
    x_center = width/2;
    y_center = height/2;
    % Extract Harris corners on original image

    C1=Harris_Angel(imgOrig);    
    
    xc=C1(:,1);
    yc=C1(:,2);
    
    numFeatures = length(xc);
    disp(['Features = ' num2str(numFeatures)]);

    % Rotate the image incrementally
    rotAngles = 0:15:360;
    %numFeatureMatches = zeros(1, numel(rotAngles));
    for nAngle = 1:length(rotAngles)
        % Rotate
        rotAngle = rotAngles(nAngle);
        disp(['Angle = ' num2str(nAngle) ':' num2str(rotAngle) ' degrees']);
        imgRot = imrotate(imgOrig, rotAngle, 'bicubic');
        [heightRot, widthRot] = size(imgRot);
        x_center_rot = widthRot/2;
        y_center_rot = heightRot/2;
        % Find Harris corners
        %[xc_rot_actual, yc_rot_actual] = harris_corners(imgRot);
        C1=Harris_Angel(imgRot);
        %cornersr = detectHarrisFeatures(imgRot);
        %C1=cornersr.gather.Location;
        xc_rot_actual=C1(:,1);
        yc_rot_actual=C1(:,2);
        %figure,imshow(imgRot);hold on;
        %plot(cornersr.gather);
        %plot(C1(:,1),C1(:,2),'*r');
        % Find feature matches to original image
        yc_rot_predict = (yc - y_center)*cosd(rotAngle) ...
            - (xc - x_center)*sind(rotAngle) + y_center_rot;
        xc_rot_predict = (yc - y_center)*sind(rotAngle) ...
            + (xc - x_center)*cosd(rotAngle) + x_center_rot;
        for nOrig = 1:length(xc_rot_predict)
            matchFound = 0;
            for nTrans = 1:length(xc_rot_actual)
                threshold = 2;
                delta_x = abs(xc_rot_predict(nOrig)-xc_rot_actual(nTrans));
                delta_y = abs(yc_rot_predict(nOrig)-yc_rot_actual(nTrans));
                if ( delta_x <= threshold ) && ( delta_y <= threshold )
                    matchFound = 1;
                    break;
                end
            end % nTrans
            if matchFound == 1
                numFeatureMatches(x-h,nAngle) = numFeatureMatches(x-h,nAngle) + 1;
            end
        end % nOrig
        disp(['Feature Matches = ' num2str(numFeatureMatches(x-h,nAngle))]);
    end % nAngle
    
end

figure(1); clf;
hold all;
for i = 1:length(numFeatureMatches(:,1));
    plot((0:15:360), numFeatureMatches(i,:) ./ numFeatures);
    %hold on;
end %for i
grid on;
xlabel('Rotation Angle (degrees)');
ylabel('Repeatability');
title('Robustness to Rotation');
legend(cell(X))
hold off;