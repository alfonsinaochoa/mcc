i=imread('graffiti/img1.ppm');

%Make image greyscale
if length(size(i)) == 3
	im =  double(i(:,:,2));
else
	im = double(i);
end

BW1 = edge(im,'sobel');  
%  edge(I,'sobel',THRESH)
%edge(I,'sobel',THRESH,DIRECTION) specifies directionality for the
%Sobel method. DIRECTION is a string specifying whether to look for
%'horizontal' or 'vertical' edges, or 'both'

BW2 = edge(im,'roberts');
%edge(I,'roberts',THRESH)

BW3 = edge(im,'prewitt');

BW4 = edge(im,'log');
%edge(I,'log',THRESH,SIGMA)   %default SIGMA is 2
% the size of the filter is N-by-N, where
% N=CEIL(SIGMA*3)*2+1.

BW5 = edge(im,'canny');
% BW = edge(I,'canny',THRESH) 
% THRESH is a two-element vector in which the first element
% is the low threshold, and the second element is the high threshold.

figure, imshow(BW1)
title('EDGES SOBEL');

figure, imshow(BW2)
title('EDGES ROBERTS');

figure, imshow(BW3)
title('EDGES PREWITT');

figure, imshow(BW4)
title('Laplacian of Gaussian method');

figure, imshow(BW5)
title('EDGES CANNY');
ginput(1);


i=imread('IR/Infra1.bmp');

%Make image greyscale
if length(size(i)) == 3
	im =  double(i(:,:,2));
else
	im = double(i);
end

BW1 = edge(im,'sobel');
BW2 = edge(im,'roberts');
BW3 = edge(im,'prewitt');
BW4 = edge(im,'log');
BW5 = edge(im,'canny');

figure, imshow(BW1)
title('EDGES SOBEL');

figure, imshow(BW2)
title('EDGES ROBERTS');

figure, imshow(BW3)
title('EDGES PREWITT');

figure, imshow(BW4)
title('Laplacian of Gaussian method');

figure, imshow(BW5)
title('EDGES CANNY');
ginput(1);

i=imread('IR/Infra2.png');
%Make image greyscale
if length(size(i)) == 3
	im =  double(i(:,:,2));
else
	im = double(i);
end

BW1 = edge(im,'sobel');
BW2 = edge(im,'roberts');
BW3 = edge(im,'prewitt');
BW4 = edge(im,'log');
BW5 = edge(im,'canny');

figure, imshow(BW1)
title('EDGES SOBEL');

figure, imshow(BW2)
title('EDGES ROBERTS');

figure, imshow(BW3)
title('EDGES PREWITT');

figure, imshow(BW4)
title('Laplacian of Gaussian method');

figure, imshow(BW5)
title('EDGES CANNY');
ginput(1);

i=imread('IR/Infra3.png');
%Make image greyscale
if length(size(i)) == 3
	im =  double(i(:,:,2));
else
	im = double(i);
end

BW1 = edge(im,'sobel');
BW2 = edge(im,'roberts');
BW3 = edge(im,'prewitt');
BW4 = edge(im,'log');
BW5 = edge(im,'canny');

figure, imshow(BW1)
title('EDGES SOBEL');

figure, imshow(BW2)
title('EDGES ROBERTS');

figure, imshow(BW3)
title('EDGES PREWITT');

figure, imshow(BW4)
title('Laplacian of Gaussian method');

figure, imshow(BW5)
title('EDGES CANNY');
ginput(1);