clear;
DENSITY = 0.05;
MEAN = 0;
VARIANCE = 0.01;
HSIZE=9;
RADIUS=5;
SIGMA=1;
ALPHA=0.7;

IMG = rgb2gray(imread('Lenna.jpg'));
imshow(IMG);
title('Imagen original en escala de grises');
hold on;
IMG = double(IMG);
originalImage = uint8(IMG);

%Poisson Noise
poisson_noise = imnoise (originalImage, 'poisson');
%Salt and Pepper Noise
salt_pepper_noise = imnoise (originalImage, 'salt & pepper', DENSITY);
%Gaussian Noise
gaussian_noise = imnoise (originalImage, 'gaussian', MEAN, VARIANCE);

%Plot
figure;
subplot(2,2,1), imshow(originalImage);
title ('Imagen original en escala de grises');
subplot(2,2,2), imshow(poisson_noise);
title ('Imagen con ruido poisson');
subplot(2,2,3), imshow(salt_pepper_noise);
title ('Imagen con ruido sal y pimienta');
subplot(2,2,4), imshow(gaussian_noise);
title ('Imagen con ruido gausiano');

%SNR Poisson Noise
snr_poisson = SNR(originalImage, (double(poisson_noise) - double(originalImage)));
fprintf('SNR usando ruido poisson = %5.5f dB \n', snr_poisson);
%SNR Salt and Pepper Noise
snr_saltpepper = SNR(originalImage,(double(salt_pepper_noise) - double(originalImage)));
fprintf('SNR usando ruido sal y pimienta = %5.5f dB \n', snr_saltpepper);
%SNR Gaussian Noise
snr_gaussian = SNR(originalImage,(double(gaussian_noise) - double(originalImage)));
fprintf('SNR usando ruido gausiano = %5.5f dB \n', snr_gaussian);

%Two dimensional filter of the specified type
%Average
h_average = fspecial('average',HSIZE);  
%f_average = imfilter(poisson_noise,h_average,'replicate','conv');
%f_average = imfilter(salt_pepper_noise,h_average,'replicate','conv');
f_average = imfilter(gaussian_noise,h_average,'replicate','conv');
%Disk
h_disk = fspecial('disk',RADIUS);
%f_disk = imfilter(poisson_noise,h_disk,'replicate','conv');
%f_disk = imfilter(salt_pepper_noise,h_disk,'replicate','conv');
f_disk = imfilter(gaussian_noise,h_disk,'replicate','conv');
%Gaussian
h_gaussian = fspecial('gaussian',HSIZE,SIGMA);
%f_gaussian = imfilter(poisson_noise,h_gaussian,'replicate','conv');
%f_gaussian = imfilter(salt_pepper_noise,h_gaussian,'replicate','conv');
f_gaussian = imfilter(gaussian_noise,h_gaussian,'replicate','conv');
%Laplacian
h_laplacian = fspecial('laplacian',ALPHA);
%f_laplacian = imfilter(poisson_noise,h_laplacian,'replicate','conv');
%f_laplacian = imfilter(salt_pepper_noise,h_laplacian,'replicate','conv');
f_laplacian = imfilter(gaussian_noise,h_laplacian,'replicate','conv');
%Log
h_log = fspecial('log',HSIZE,SIGMA);
%f_log = imfilter(poisson_noise,h_log,'replicate','conv');
%f_log = imfilter(salt_pepper_noise,h_log,'replicate','conv');
f_log = imfilter(gaussian_noise,h_log,'replicate','conv');
%Prewitt
h_prewitt = fspecial('prewitt');
%f_prewitt = imfilter(poisson_noise,h_prewitt,'replicate','conv');
%f_prewitt = imfilter(salt_pepper_noise,h_prewitt,'replicate','conv');
f_prewitt = imfilter(gaussian_noise,h_prewitt,'replicate','conv');
%Sobel
h_sobel = fspecial('sobel');
%f_sobel = imfilter(poisson_noise,h_sobel,'replicate','conv');
%f_sobel = imfilter(salt_pepper_noise,h_sobel,'replicate','conv');
f_sobel = imfilter(gaussian_noise,h_sobel,'replicate','conv');
%Unsharp
h_unsharp = fspecial('unsharp',ALPHA);
%f_unsharp = imfilter(poisson_noise,h_unsharp,'replicate','conv');
%f_unsharp = imfilter(salt_pepper_noise,h_unsharp,'replicate','conv');
f_unsharp = imfilter(gaussian_noise,h_unsharp,'replicate','conv');

%Plot
figure;
subplot(2,2,1), imshow(f_average);
title ('Imagen con filtro average');
subplot(2,2,2), imshow(f_disk);
title ('Imagen con filtro disk');
subplot(2,2,3), imshow(f_gaussian);
title ('Imagen con filtro gausiano');
subplot(2,2,4), imshow(f_laplacian);
title ('Imagen con filtro laplaciano');

figure;
subplot(2,2,1), imshow(f_log);
title ('Imagen con filtro log');
subplot(2,2,2), imshow(f_prewitt);
title ('Imagen con filtro prewitt');
subplot(2,2,3), imshow(f_sobel);
title ('Imagen con filtro sobel');
subplot(2,2,4), imshow(f_unsharp);
title ('Imagen con filtro unsharp');

%SNR Average Filter
snr_faverage = SNR(originalImage,(double(f_average) - double(originalImage)));
fprintf('SNR usando filtro promedio = %5.5f dB \n', snr_faverage);
%SNR Disk Filter
snr_fdisk = SNR(originalImage, (double(f_disk) - double(originalImage)));
fprintf('SNR usando filtro disk = %5.5f dB \n', snr_fdisk);
%SNR Gaussian Noise
snr_fgaussian = SNR(originalImage, (double(f_gaussian) - double(originalImage)));
fprintf('SNR usando filtro gausiano = %5.5f dB \n', snr_fgaussian);
%SNR Laplacian Noise
snr_flaplacian = SNR(originalImage, (double(f_laplacian) - double(originalImage)));
fprintf('SNR usando filtro laplaciano = %5.5f dB \n', snr_flaplacian);
%SNR Laplacian Noise
snr_flog = SNR(originalImage, (double(f_log) - double(originalImage)));
fprintf('SNR usando filtro log = %5.5f dB \n', snr_flog);
%SNR Laplacian Noise
snr_fprewitt = SNR(originalImage, (double(f_prewitt) - double(originalImage)));
fprintf('SNR usando filtro prewitt = %5.5f dB \n', snr_fprewitt);
%SNR Laplacian Noise
snr_fsobel = SNR(originalImage, (double(f_sobel) - double(originalImage)));
fprintf('SNR usando filtro sobel = %5.5f dB \n', snr_fsobel);
%SNR Laplacian Noise
snr_funsharp = SNR(originalImage,(double(f_unsharp) - double(originalImage)));
fprintf('SNR usando filtro unsharp = %5.5f dB \n', snr_funsharp);


