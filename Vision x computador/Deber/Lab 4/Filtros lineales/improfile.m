%Copyright (C) 2005  Estrade-Mercadier

%This program is free software; you can redistribute it and/or
%modify it under the terms of the GNU General Public License
%as published by the Free Software Foundation; either version 2
%of the License, or (at your option) any later version.

%This program is distributed in the hope that it will be useful,
%but WITHOUT ANY WARRANTY; without even the implied warranty of
%MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%GNU General Public License for more details.

function [R,cx,cy,x,y] = improfile(varargin)

##   IMPROFILE This function makes it possible to visualize on 
##   graphics the values of the intensity of the pixels which are 
##   located on a line or several segments of straight lines.
##   These lines must be to give by the user in the form of vectors
##   (see example).
##
##   If no argument is asked in exit, the graph of the intensities 
##   displays. Otherwise, it is possible to get back the values 
##   measured by giving only a parameter of exit.
##
##   You can also specify the path noninteractively, using these
##   syntaxes:
##
##        R = IMPROFILE(nb,r,xi,yi)
##        R = IMPROFILE(nb,r,..,xi,yi,n)
##
##   nb is the numbers of colors of the image.
##   r,g,b are the three components of the image.
##   xi and yi are equal-length vectors specifying the spatial
##   coordinates of the endpoints of the line segments.
##   n is the hopped number of out points.
##
##   You can use these syntaxes to return additional information:
##
##        [R,cx,cy] = IMPROFILE(...)
##        [R,cx,cy,xi,yi] = IMPROFILE(...)
##
##   cx and cy are vectors of length N, containing the spatial
##   coordinates of the points at which the intensity values are
##   computed.
##   xi and yi are the control points of the curve.
##
##
##   IMPROFILE uses by default the bilinear interpolaion method.
##
##   Functions necessary to use IMPROFILE
##   ------------------------------------
##   transpose.m
##   size.m
##   sqrt.m
##   sum.m
##   plot.m
##
##   Example for a colour image 
##   --------------------------
##        [r,g,b] = imread('image.jpg');
##        x = [35 338 346 103];
##        y = [253 250 17 148];
##        improfile(3,r,g,b,x,y), grid on
##


## -*- texinfo -*-
##
## @deftypefn {Function File} {@var{R} = } improfile (@var{nb}, @var{r}, @var{g}, @var{b}, @var{x}, @var{y} , @var{n})
## @deftypefnx {Function File} {@var{R} = } improfile (@var{nb}, @var{r}, @var{g}, @var{b}, @var{x}, @var{y})
## @deftypefnx {Function File} {@var{R} = } improfile (@var{nb}, @var{r}, @var{x}, @var{y}, @var{n})
## @deftypefnx {Function File} {@var{R} = } improfile (@var{nb}, @var{r}, @var{x}, @var{y})
##
## Input arguments :
##
##      @var{nb} is the number of color of the image
##      @var{r} is the red component of the image or the grey level of
##          an image.
##      @var{g} is the green component of the image
##      @var{b} is the blue component of the image
##      @var{x} is the vector of X-coordinates of control points
##      @var{x} is the vector of Y-coordinates of control points
##      @var{n} is the number of output points
##
##   $Date: 2005/03/25 13:20:10 $
##
## @end deftypefn



    nc = varargin{1};
    r = varargin{2};

% test of the parametres which are give in input 
if nc==3
    	g = varargin{3};
    	b = varargin{4};
    
    	x = varargin{5};
		y = varargin{6};
		if (nargin==7)
			n = varargin{7};
		end
else
	x = varargin{3};
	y = varargin{4};
	if (nargin==5)
		n = varargin{5};
    end
end

    % sizes of coordinates vectors
    s1=size(x);s2=size(y);

    s=1;
% tests about the size of the coordinates vectors
    if (s1(2)==s2(2))
    	% calculation of the distance
        for i=1:s1(2)-1
            d(i) = sqrt((x(i)-x(i+1))^2+(y(i)-y(i+1))^2);
        end
        % total length traversed
        l = sum(d);
        
        dc = 0;
        cc = 1;
	

		for i=1:s1(2)-1
		
			dx = x(i+1)-x(i);
			dy = y(i+1)-y(i);
		
		
			for s = 0:d
		
				ch(dc+s+1,1) = r( ceil(y(i)+dy*s/d(i)) , ceil(x(i)+dx*s/d(i)) );
			
				if nc==3
					ch(dc+s+1,2) = g( ceil(y(i)+dy*s/d(i)) , ceil(x(i)+dx*s/d(i)) );
					ch(dc+s+1,3) = b( ceil(y(i)+dy*s/d(i)) , ceil(x(i)+dx*s/d(i)) );
				end
			
				cy(cc) = y(i)+dy*s/d(i);
				cx(cc) = x(i)+dx*s/d(i);
			
				cc = cc+1;
		
			end
		
			dc = dc+d-1;
			cc = cc-1;
		
		end
            
        ch(cc,1) = r( y(s1(2)) , x(s1(2)) );
		if nc==3
		    ch(cc,2) = g( y(s1(2)) , x(s1(2)) );
		    ch(cc,3) = b( y(s1(2)) , x(s1(2)) );
		end
		
		
        if (nargin==5 & nc==1) | (nargin==7)
            
            for k=1:n
                R(k,1) = ch(k*l/n,1);
                if nc==3
			R(k,2) = ch(k*l/n,2);
			R(k,3) = ch(k*l/n,3);
		end
            end
            
        else
            R = ch;
        end
  
    else
	    printf('Error: the vectors y and x don t have the same size');   
    end
    
    % ploting of the result if no output arguments
    if (nargout == 0)
        
        figure;
        
        if nc == 3
		    plot(ch(:,1),"1",ch(:,2),"2",ch(:,3),"3");
        else
		    plot(R);
        end
        
    end