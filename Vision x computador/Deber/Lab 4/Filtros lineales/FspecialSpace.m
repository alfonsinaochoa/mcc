DENSITY = 0.05;
MEAN = 0;
VARIANCE = 0.01;

im = imread('Lenna.jpg');
img = rgb2gray(im);
imshow(img);
title('Imagen original en escala de grises');
hold on;

%Poisson Noise
poisson_noise = imnoise (img, "poisson");
%Salt and Pepper Noise
salt_pepper_noise = imnoise (img, "salt & pepper", DENSITY);
%Gaussian Noise
gaussian_noise = imnoise (img, "gaussian", MEAN, VARIANCE);

figure;
subplot(1,2,1), imshow(img);
title ('Imagen original en escala de grises');
subplot(1,2,2), imshow(poisson_noise);
title ('Imagen con ruido poisson');
subplot(2,1,1), imshow(salt_pepper_noise);
title ('Imagen con ruido sal y pimienta');
subplot(2,1,2), imshow(gaussian_noise);
title ('Imagen con ruido gausiano');

