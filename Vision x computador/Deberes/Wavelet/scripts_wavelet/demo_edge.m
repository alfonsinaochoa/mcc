I=imread('figures.png');
figure,imshow(I);
escalas=4;
w='db4';

Iwd = fwt2(double(I),w,escalas);
figure, imagesc((20*log10(abs(Iwd)))),colormap(gray);

wd=wdcomp2s(Iwd,escalas);
%wdhist_plot(wd);
wd{4,4}.coeff = 0;
Iwd=wdsyn2s(wd);
Ir=ifwt2(Iwd,w,escalas);
Ir=uint8(Ir);
%figure,imshow(Ir);

wdf=wd;
wdf=wdfilts(wdf,0.5,1);
wdf=wdfilts(wdf,0.5,2);
wdf=wdfilts(wdf,0.5,3);
wdf=wdfilts(wdf,0.5,4);
wcf=wdsyn2s(wdf);
figure, imagesc((20*log10(abs(wcf))),[1 70]),colormap(gray);
Irf=ifwt2(wcf,w,escalas);

Irf=uint8(Irf);
figure,imshow(Irf);
imwrite(Irf,'edge_result.png');
