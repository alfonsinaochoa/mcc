I1=imread('batman.png');
I2=imread('smoke.png');

escalas=1;
w='db4';

Iwd1 = fwt2(double(I1),w,escalas);
Iwd2 = fwt2(double(I2),w,escalas);

wd1=wdcomp2s(Iwd1,escalas);
wd2=wdcomp2s(Iwd2,escalas);

%combinacion lineal de los coeficientes de aproximacion
comb = imlincomb(1,wd1{1,4}.coeff,1,wd2{1,4}.coeff); 
wd=wd1;
%reemplazar los coeficientes resultantes en la primera imagen
wd{1,4}.coeff = comb;

Iwd=wdsyn2s(wd);
Ir=ifwt2(Iwd,w,escalas);
Ir=uint8(Ir);

wdf=wd;
wdf=wdfilts(wdf,2,1);
wdf=wdfilts(wdf,2,2);
wdf=wdfilts(wdf,1,3);
wdf=wdfilts(wdf,1,4);
wcf=wdsyn2s(wdf);
figure, imagesc((20*log10(abs(wcf))),[1 70]),colormap(gray);
Irf=ifwt2(wcf,w,escalas);

Irf=uint8(Irf);
figure,imshow(Irf);
imwrite(Irf,'fusion_result.png');
