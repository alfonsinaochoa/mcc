function [wc]=wdcomp2s(wd,A)

[r c]=size(wd);
wc=cell(A,4);

for i=[1:A]
	midr=round(r/2);
	midc=round(c/2);
	dcomp.sr=1;
	dcomp.sc=midc+1;
	dcomp.coeff=wd(1:midr,midc+1:c);
        wc(i,1)=dcomp;

	dcomp.sr=midr+1;
	dcomp.sc=midc+1;
	dcomp.coeff=wd(midr+1:r,midc+1:c);
        wc(i,2)=dcomp;

	dcomp.sr=midr+1;
	dcomp.sc=1;
	dcomp.coeff=wd(midr+1:r,1:midc);
        wc(i,3)=dcomp;

	if (i==A)
		
		dcomp.sr=1;
		dcomp.sc=1;
		dcomp.coeff=wd(1:midr,1:midc);
	        wc(i,4)=dcomp;
 	endif
	r=midr;
	c=midc;
endfor

endfunction

