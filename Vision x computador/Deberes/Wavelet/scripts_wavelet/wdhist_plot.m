function wdhist_plot(wc)

s=size(wc,1);%number of scales used for decomposition
rs=50;
figure; 
for i=[s:-1:1]
  	h1=reshape(wc{i,1}.coeff,1,size(wc{i,1}.coeff,1)*size(wc{i,1}.coeff,2));
	h2=reshape(wc{i,2}.coeff,1,size(wc{i,2}.coeff,1)*size(wc{i,2}.coeff,2));
	h3=reshape(wc{i,3}.coeff,1,size(wc{i,3}.coeff,1)*size(wc{i,3}.coeff,2));
	subplot(s,3,3*(s-i)+1);
	hist(h1,rs);
	title(gca,strcat("Vertical - a=",num2str(i)));
	subplot(s,3,3*(s-i)+2);
	hist(h2,rs);
	title(gca,strcat("Diagonal - a=",num2str(i)));
	subplot(s,3,3*(s-i)+3);
	hist(h3,rs);
	title(gca,strcat("Horizontal - a=",num2str(i)));
  
endfor

endfunction
