function [wc]=wdfilts(wc,th,i)

s=size(wc,1);%number of scales used for decomposition
if s>=i
	     wc{i,1}.coeff((abs(wc{i,1}.coeff)<th))=0;
	     wc{i,2}.coeff((abs(wc{i,2}.coeff)<th))=0;
	     wc{i,3}.coeff((abs(wc{i,3}.coeff)<th))=0;

endif

endfunction
