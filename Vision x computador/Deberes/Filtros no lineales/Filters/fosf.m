% Example of First order statistics filter (fosf) 
clear;
DENSITY = 0.05;
VARIANCE = 0.02;
I = imread('images/sar.png');
%I = rgb2gray(imread('images/sar.png'));
inputImage = imnoise(I,'speckle',VARIANCE);
outputImage1 = fcnFirstOrderStatisticsFilter(inputImage);
figure(1), subplot 121, imshow(inputImage),title ('Speckle noise');
subplot 122,imshow(outputImage1),title ('First order statistics filter');
inputImage2 = imnoise(I,'salt & pepper',DENSITY);
outputImage2 = fcnFirstOrderStatisticsFilter(inputImage2);
figure(2), subplot 121, imshow(inputImage2), title ('Salt and pepper noise');
subplot 122, imshow(outputImage2),title ('First order statistics filter');