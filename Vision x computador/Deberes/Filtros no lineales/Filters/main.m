clear;
DENSITY = 0.05;
VARIANCE = 0.02;
M = 3;
N = 3;
I = imread('images/sar.png');
%I = rgb2gray(imread('images/sar.png'));
IMG = double(I);
originalImage = uint8(IMG);
%Add Noise
salt_pepper_noise = imnoise (I, 'salt & pepper', DENSITY);
speckle_noise = imnoise (I, 'speckle', VARIANCE);
%uses a 3-by-3 neighborhood - Median Filter
outputImage1 = imrest(speckle_noise,'median',M,N); 
outputImage2 = imrest(salt_pepper_noise,'median',M,N); 
%Midpoint Filter
outputImage3 = imrest(speckle_noise,'midpoint',M,N);
outputImage4 = imrest(salt_pepper_noise,'midpoint',M,N);
%SNR Noise
% [peaksnr, snr] = psnr(salt_pepper_noise, I);
% fprintf('\n Peak-SNR usando usando ruido sal y pimienta %0.4f', peaksnr);
% fprintf('\n SNR usando usando ruido sal y pimienta %0.4f \n', snr);
% 
% [peaksnr, snr] = psnr(speckle_noise, I);
% fprintf('\n Peak-SNR usando usando ruido speckle %0.4f', peaksnr);
% fprintf('\n SNR usando usando ruido speckle %0.4f \n', snr);

%SNR Filter
[peaksnr, snr] = psnr(outputImage1, I);
fprintf('\n Peak-SNR usando usando filtro mediano con ruido speckle %0.4f', peaksnr);
fprintf('\n SNR usando usando filtro mediano con ruido speckle %0.4f \n', snr);

[peaksnr, snr] = psnr(outputImage2, I);
fprintf('\n Peak-SNR usando usando filtro mediano con ruido sal y pimienta %0.4f', peaksnr);
fprintf('\n SNR usando usando filtro mediano con ruido sal y pimienta %0.4f \n', snr);

[peaksnr, snr] = psnr(outputImage3, I);
fprintf('\n Peak-SNR usando usando filtro midpoint con ruido speckle %0.4f', peaksnr);
fprintf('\n SNR usando usando filtro midpoint con ruido speckle %0.4f \n', snr);

[peaksnr, snr] = psnr(outputImage4, I);
fprintf('\n Peak-SNR usando usando filtro midpoint con ruido sal y pimienta %0.4f', peaksnr);
fprintf('\n SNR usando usando filtro midpoint con ruido sal y pimienta %0.4f \n', snr);

%Plot
figure(1),subplot 131, imshow(speckle_noise),title ('Speckle noise');
subplot 132,imshow(outputImage1),title ('Median filter');
subplot 133,imshow(outputImage3),title ('Midpoint filter');
figure(2),subplot 131, imshow(salt_pepper_noise),title ('Salt and pepper noise');
subplot 132,imshow(outputImage2),title ('Median filter');
subplot 133,imshow(outputImage4),title ('Midpoint filter');

