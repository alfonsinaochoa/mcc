clear;
M = 3;
N = 3;
originalImage = imread('images/sar.png');
%Midpoint filter
outputImage1 = imrest(originalImage,'midpoint',M,N) ;
figure(1),imshow(originalImage),title ('Original image');
figure(2),imshow(outputImage1),title ('Midpoint filter');

structure = strel('square', M);
%outputImage2 = imdilate(outputImage1,structure);
%outputImage3 = imerode(outputImage2,structure);
outputImage2 = imopen(outputImage1,structure);
figure(3),imshow(outputImage2),title ('Open');
%[x,y] = ginput(4)
binaryImage = im2bw(outputImage2);
[LOutputImage, countOutput] = bwlabel(binaryImage);
[LFilterImage, countFilter] = bwlabel(im2bw(outputImage1));
[LOriginalImage, countOriginal] = bwlabel(im2bw(originalImage));

