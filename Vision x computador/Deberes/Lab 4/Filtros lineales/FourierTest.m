function [] = FourierTest(im)

%Convert to grayscale
im=rgb2gray(im); 

% Calculate the discrete Fourier transform of the image
F=fft2(double(im));
fftshow(fftshift(F));

% convert the result to the spacial domain.
F_im=real(ifft2(F)); 

ifftshow(F_im);

end