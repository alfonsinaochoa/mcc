%Copyright (C) 2005  Estrade-Mercadier

%This program is free software; you can redistribute it and/or
%modify it under the terms of the GNU General Public License
%as published by the Free Software Foundation; either version 2
%of the License, or (at your option) any later version.

%This program is distributed in the hope that it will be useful,
%but WITHOUT ANY WARRANTY; without even the implied warranty of
%MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%GNU General Public License for more details.


%[r,g,b] = imread('Lenna.jpg');
%imshow(r,g,b);
I = imread('Lenna.jpg');
imshow(I);

x = [50 60 60]
y = [50 50 60]

%improfile(3,r,g,b,x,y);
improfile(I,x,y),grid on;

%[R,xc,yc,x,y] = improfile(3,r,g,b,x,y);

