function [] = filterAlpha(I, type1, type2, alpha)

%Convert to grayscale
I=rgb2gray(I); 
figure,imshow(I),title ('Imagen original en escala de grises');

%Determine good padding for Fourier transform
PQ = paddedsize(size(I)); 

%Create a filter 5% the width of the Fourier transform
D0 = 0.05*PQ(1); 

LF = lpfilter(type1, PQ(1), PQ(2), D0);
HF = hpfilter(type2, PQ(1), PQ(2), D0);

%fftshow(fftshift(LF));
%fftshow(fftshift(HF));

% Calculate the discrete Fourier transform of the image
FI=fft2(double(I),size(LF,1),size(LF,2));
FI2=fft2(double(I),size(HF,1),size(HF,2));

% Apply the filter to the Fourier spectrum of the image
FF_im = LF.*FI;
FF_im2 = HF.*FI;

%FR= alpha*F' + (1-alpha)*F'
FR = alpha * FF_im + (1 - alpha) * FF_im2;

% convert the result to the spacial domain.
F_im=real(ifft2(FR)); 

% Crop the image to undo padding
F_im=F_im(1:size(I,1), 1:size(I,2));

%Display the blurred image
figure, imshow(F_im, [])
title (strcat('Imagen con filtro pasa bajo tipo ' , {' '} , type1 , ' ' ,' y filtro paso alto tipo' , {' '} , type2 , ' con valor de alpha ', {' '} , num2str(alpha)));
end
