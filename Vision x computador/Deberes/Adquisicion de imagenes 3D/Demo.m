clear; close all;
IM1=imread('Image1.tif');
IM2=imread('Image2.tif');
IM3=imread('Image3.tif');

I1=rgb2gray(IM1);
I2=rgb2gray(IM2);
I3=rgb2gray(IM3);

var1= double(I1);
var2= double(I2);
var3= double(I3);

v1=var(var1,0,2);
v2=var(var2,0,1);
v3=var(var3);
[k l] = size(I2);
variance1 = zeros(size(I2));
variance2 = zeros(size(I2));
variance3 = zeros(size(I2));

for i = 2:k-1
    for j = 2:l-1      
        x1 = I1(i-1:i+1, j-1:j+1);
        x2 = I2(i-1:i+1, j-1:j+1);
        x3 = I3(i-1:i+1, j-1:j+1);
        variance1(i,j) = var(double(x1(:)));
        variance2(i,j) = var(double(x2(:)));
        variance3(i,j) = var(double(x3(:)));        
    end;    
end;

multifocal_matrix = IM2; %zeros(size(I2));
depth_matrix = zeros(size(I2));
%distancia focal se asume que es 1
%1/f = 1/u + 1/v
v1=3;
v2=2;
v3=4;
u1 = 1/(1 - 1/v1);
u2 = 1/(1 - 1/v2);
u3 = 1/(1 - 1/v3);
for i = 2:k-1
    for j = 2:l-1    
        if(variance1(i,j) >= variance2(i,j) && variance1(i,j) >= variance3(i,j))
            %multifocal_matrix(i,j) = IM1(i,j);
            depth_matrix(i,j) = u1;
            for c=1:3
                multifocal_matrix(i,j,c)=IM1(i,j,c);
            end
        elseif(variance2(i,j) >= variance1(i,j) && variance2(i,j) >= variance3(i,j))
            %multifocal_matrix(i,j) = IM2(i,j);
            depth_matrix(i,j) = u2;
            for c=1:3
                multifocal_matrix(i,j,c)=IM2(i,j,c);
            end
        else
            %multifocal_matrix(i,j) = IM3(i,j);
            depth_matrix(i,j) = u3;
            for c=1:3
                multifocal_matrix(i,j,c)=IM3(i,j,c);
            end
        end
    end
end

%figure, imshow(multifocal_matrix);
%figure, imshow(mat2gray(multifocal_matrix));
figure, surf(depth_matrix,multifocal_matrix,'FaceColor','texturemap','EdgeColor','none');
rotate3d on;