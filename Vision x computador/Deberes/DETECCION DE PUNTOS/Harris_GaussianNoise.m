mean=0.1; 
variance=0.01; 
threshold = 2;
h=2;

imageFiles = dir('images/*');
parameter_array = 0:0.01:0.05;
arrayImages = java_array('java.lang.String', length(imageFiles) -3);
numFeatureMatches = zeros(length(imageFiles)- h,length(parameter_array));

for image = h+1:length(imageFiles)
    fname = imageFiles(image).name;    
    arrayImages(image-h) = java.lang.String(fname);   
    OriginalImage = imread(fullfile('images/',imageFiles(image).name));
    [height, width] = size(OriginalImage);
 
     % Extract Harris corners on original image
     corners = Harris_Angel(OriginalImage);
     x_corner=corners(:,1);
     y_corner=corners(:,2);
     numFeatures = length(y_corner);
 
     for m = 1:length(parameter_array) 
         %Add Gaussian Noise to the original image
         GaussianNoiseImg = imnoise(OriginalImage,'gaussian',parameter_array(m),0);
         [heightG, widthG] = size(GaussianNoiseImg);
         corners_noise=Harris_Angel(GaussianNoiseImg);
         x_corner_noise=corners_noise(:,1);
         y_corner_noise=corners_noise(:,2);
         numFeatures_noise = length(y_corner_noise);
 
         for i = 1:numFeatures
            matchPoint = 0;
            for j = 1:numFeatures_noise
                deltaX = abs(x_corner(i)-x_corner_noise(j));
                deltaY = abs(y_corner(i)-y_corner_noise(j));
                if ( deltaX <= threshold ) && ( deltaY <= threshold )
                    matchPoint = 1;
                    break;
                end %end if
            end % end for j
            if matchPoint == 1
                numFeatureMatches(image-h, m) = numFeatureMatches(image-h, m) + 1;
            end % end if
        end % end for i
    end %end for m

    %Plot Images
%     figure,imshow(OriginalImage);
%     hold
%     plot(corners(:,1),corners(:,2),'*r');
%     title('HARRIS Corner Points Original Image');
%     ginput(1);
%     figure,imshow(GaussianNoiseImg);
%     hold
%     plot(corners_noise(:,1),corners_noise(:,2),'*r');
%     title('HARRIS Corner Points Image With Gaussian Noise ');
%     ginput(1);

figure(1); clf;
hold all;
for k = 1:length(numFeatureMatches(:,1));
    plot((parameter_array), numFeatureMatches(k,:) ./ numFeatures);
end %for i
grid on;
xlabel('Mean and Variance Gaussian Noise');
ylabel('Repeatability');
title('Robustness to Gaussian Noise');
end %end for
legend(cell(arrayImages));
hold off;