threshold = 2;
h=2;

imageFiles = dir('rgbimages/*');
parameter_array = 0.5:0.5:5;
%parameter_array = [-0.05 1 2 0.08 0.4 10]
arrayImages = java_array('java.lang.String', length(imageFiles) -3);
numFeatureMatches = zeros(length(imageFiles)- h,length(parameter_array));

for image = h+1:length(imageFiles)
    fname = imageFiles(image).name;    
    arrayImages(image-h) = java.lang.String(fname);   
    OriginalImage = imread(fullfile('rgbimages/',imageFiles(image).name));       
     % Extract Harris corners on original image
     corners = Harris_Angel(OriginalImage);
     x_corner=corners(:,1);
     y_corner=corners(:,2);
     numFeatures = length(y_corner);
  
     for m = 1:length(parameter_array)          
         %Convert to HSV image
         HSVImage = rgb2hsv(OriginalImage);
         imEscalada = HSVImage(:,:,1);
         imEscalada(:,:,2) = HSVImage(:,:,2);
         imEscalada(:,:,3) = HSVImage(:,:,3)*parameter_array(m);
         RGBImage = hsv2rgb(imEscalada);
         % Extract Harris corners on image
         corners_noise=Harris_Angel(RGBImage);
         x_corner_noise=corners_noise(:,1);
         y_corner_noise=corners_noise(:,2);
         numFeatures_noise = length(y_corner_noise);
 
         for i = 1:numFeatures
            matchPoint = 0;
            for j = 1:numFeatures_noise
                deltaX = abs(x_corner(i)-x_corner_noise(j));
                deltaY = abs(y_corner(i)-y_corner_noise(j));
                if ( deltaX < threshold ) && ( deltaY < threshold )                   
                    matchPoint = 1;
                    break;
                end %end if
            end % end for j
            if matchPoint == 1
                numFeatureMatches(image-h, m) = numFeatureMatches(image-h, m) + 1;
            end % end if
        end % end for i
    end %end for m

    %Plot Images
%     figure,imshow(OriginalImage);
%     hold
%     plot(corners(:,1),corners(:,2),'*r');
%     title('HARRIS Corner Points Original Image');
%     ginput(1);
%     figure,imshow(RGBImage);
%     hold
%     plot(corners_noise(:,1),corners_noise(:,2),'*r');
%     title('HARRIS Corner Points Image With HSV-RGB Image');
%     ginput(1);

    figure(1); clf;
    hold all;
    for k = 1:length(numFeatureMatches(:,1));
        plot((parameter_array), numFeatureMatches(k,:) ./ numFeatures);
    end %for i
    grid on;
    xlabel('Range of Illumination');
    ylabel('Repeatability');
    title('Robustness to Illumination');
end %end for
legend(cell(arrayImages));
hold off;