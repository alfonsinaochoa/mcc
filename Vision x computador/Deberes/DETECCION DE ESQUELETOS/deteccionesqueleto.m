%i=imread('Images_Skeleton/chicken-16.gif'); 
%i=imread('Images_Skeleton/rat-15.gif'); 
%i=imread('Images_Skeleton/bat-14.gif'); 
%i=imread('Images_Skeleton/octopus-8.gif'); 
%i=imread('Images_Skeleton/device0-19.gif'); 
i=imread('Images_Skeleton/pencil-15.gif'); 

%Make image greyscale
if length(size(i)) == 3
	im =  double(i(:,:,2));
else
	im = double(i);
end

%bw = edge(im,'sobel',0.5;  
%bw = edge(im,'Prewitt');  
bw = edge(im,'Canny',0.05);
%bw = edge(im,'Roberts',0.5);  

figure, imshow(bw);
title('EDGES');
D1 = bwdist(bw,'euclidean');
D2 = bwdist(bw,'cityblock');
D3 = bwdist(bw,'chessboard');
D4 = bwdist(bw,'quasi-euclidean');
figure, imshow(mat2gray(D1)), title('Euclidean Distance Transform')
figure, imshow(mat2gray(D2)), title('City block Distance Transform')
figure, imshow(mat2gray(D3)), title('Chessboard Distance Transform')
figure, imshow(mat2gray(D4)), title('Quasi-Euclidean Distance Transform')