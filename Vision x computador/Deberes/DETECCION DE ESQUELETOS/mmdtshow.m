function y =  mmdtshow( f, n )
%  MMDTSHOW Display a distance transform image with an iso-line color table.
%
%  Y =  MMDTSHOW( F, N )
%
%  Input:
%    F - Gray-scale (uint8 or uint16) image. Distance transform.
%    N - Boolean. Number of iso-contours. DEFAULT: 10.
%
%  Output:
%    Y - Gray-scale (uint8 or uint16) or binary image (logical uint8).
%        Optionally return RGB uint8 image
%
%  Displays the distance transform image F (uint8 or uint16) with a
%    special gray-scale color table with N pseudo-color equaly spaced.
%    The final appearance of this display is similar to an iso-contour
%    image display. The infinity value, which is the maximum level
%    allowed in the image, is displayed as black. The image is
%    displayed in the MATLAB figure only if no output parameter is
%    given.
%
%  Examples
%  --------
%
%  f=mmreadgray('blob.tif');
%  fd=mmdist(f);
%  mmshow(fd);
%  mmdtshow(fd);
%
%
%  See also MMDIST, MMGDIST, MMLBLSHOW, MMSHOW, MMSURF.

% begin of the default argument automatic treatment

if ~exist('n','var') n = 10; end
% end of the default argument automatic treatment 1

if (nargin < 1)
   disp('Error, mmdtshow: needs at least one input image.');
   return;
end
if (mmisbinary(f))
  disp('Error, mmdtshow: works only for gray-scale image');
  return;
end
y=mmgdtshow(f,n);
if nargout == 0
   newFigure=isempty(get(0,'CurrentFigure')) | strcmp(get(get(0,'CurrentFigure'), 'NextPlot'), 'new');
   if (newFigure)
      figHandle = figure('Visible', 'off');
      axHandle = axes('Parent', figHandle);
   else
      axHandle = newplot;
      figHandle = get(axHandle, 'Parent');
   end
   image(y,'Parent',axHandle);
   axis off;
   set(axHandle,'DataAspectRatio',[1 1 1]);
   if (length(findobj(axHandle,'Type','image')) <= 1)
      figKids=allchild(figHandle);
      kids=[findobj(figKids,'Type','axes') ; findobj(figKids,'Type','uicontrol','Visible','on')];
      if (length(kids) <= 1)
         if (isequal(get(axHandle,'Position'),get(get(axHandle,'Parent'),'DefaultAxesPosition')))
            mmtruesize(f); % change the figure size to reflect image size
            set(figHandle,'NextPlot','replacechildren');
         end
      end
   end
   figure(gcf);
end


%  Copyright (c) 1998-2001 by SDC Information Systems.        
