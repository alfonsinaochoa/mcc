i=imread('Images_Skeleton/img1.ppm');

%Make image greyscale
if length(size(i)) == 3
	im =  double(i(:,:,2));
else
	im = double(i);
end

bw = edge(im,'sobel');  
figure, imshow(bw);
title('EDGES SOBEL');
ginput(1);


D1 = bwdist(bw,'euclidean');
D2 = bwdist(bw,'cityblock');
D3 = bwdist(bw,'chessboard');
D4 = bwdist(bw,'quasi-euclidean');

figure
subplot(2,2,1), subimage(mat2gray(D1)), title('Euclidean Distance Transform')
hold on, imcontour(D1)
subplot(2,2,2), subimage(mat2gray(D2)), title('City block Distance Transform')
hold on, imcontour(D2)
subplot(2,2,3), subimage(mat2gray(D3)), title('Chessboard Distance Transform')