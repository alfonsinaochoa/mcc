
% 
% MATLAB code that performs Homomorphic filtering, Using Butterworth
% High Pass Filter for performing filtering.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc
close all
clear all
% Usage BUTTERWORTHBPF(I,DO,D1,N)
% Example
ima = imread('Lenna.jpg');
ima = rgb2gray(ima);
filtered_image = butterworthbpf(ima,30,100,4);


