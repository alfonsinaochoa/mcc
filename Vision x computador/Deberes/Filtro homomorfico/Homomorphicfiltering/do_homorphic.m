% program for homomorphic filtering
filename= 'Lenna.jpg'; % the filename to be read
%im=rawread(filename, 256, 256);
im = imread(filename);
figure(1);
%Imagesc(im);  % display
%axis('square');
%colormap('gray');

%newim = homomorphic(image, 2, .25, 2, 0, 5);


[rows,cols] = size(im);

    cutoff=0.1; % needed for the homomorphic filter
    n=1.0;
    boost=3;
    %-------------------------------------------
    % X and Y matrices with ranges normalised to +/- 0.5
    x =  (ones(rows,1) * [1:cols]  - (fix(cols/2)+1))/cols;
    y =  ([1:rows]' * ones(1,cols) - (fix(rows/2)+1))/rows;
    
    radius = sqrt(x.^2 + y.^2);        % A matrix with every pixel = radius relative to centre.
    
    filtlow=1 ./ (1.0 + (radius ./ cutoff).^(2*n)) ;
    filthigh= 1.0 - filtlow;
    filthboost = (1-1/boost)* filthigh+ 1/boost;

    h = fftshift(filthboost );   % The highboost filter
    %----------------------------------------------


    
    
    im = normalise(im);                        % Rescale values 0-1 (and cast
					       % to `double' if needed).
    FFTlogIm = fft2(log(im+.01));              % Take FFT of log (with offset
                                               % to avoid log of 0).
                                               
                                              
                                             
                                               
    him = exp(real(ifft2(FFTlogIm.*h)));       % Apply the high boost filter, invert
					       % fft, and invert the log.


   figure(2);
   Imagesc(him);  % display
   axis('square');
   colormap('gray');
