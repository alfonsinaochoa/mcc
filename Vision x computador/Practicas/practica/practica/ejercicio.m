clear;
pkg load image
im = imread('sar.png');#con imagen gif no se cargaba bien


#FILTRO NO LINEAL
F = imrest(im,'midpoint',3,3) ; #filtro mdpoint, uno de los del paper

#FILTRO MORFOLOGICO
MN= [4;6];#tamaño para la figura
SE = strel('rectangle', MN);#definición de la figura


#prueba, dilatar y luego erosinar la imagen filtrada
IM3 = imdilate(F,SE);
IM2 = imerode(IM3,SE);
figure 1, imshow(IM2);

#prueba, dilatar y luego erosinar la imagen original
IM3 = imdilate(im,SE);
IM2 = imerode(IM3,SE);
figure 2, imshow(IM2);

#contar las estructuras en ambas imagenes