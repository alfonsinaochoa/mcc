I=imread('figures.png');
figure,imshow(I);
escalas=4;
w='db4';

Iwd = fwt2(double(I),w,escalas);
figure, imagesc((20*log10(abs(Iwd)))),colormap(gray);

wd=wdcomp2s(Iwd,escalas);
wdhist_plot(wd);
Iwd=wdsyn2s(wd);
Ir=ifwt2(Iwd,w,escalas);
Ir=uint8(Ir);
figure,imshow(Ir);

wdf=wd;
wdf=wdfilts(wdf,2,1);
wdf=wdfilts(wdf,2,2);
wdf=wdfilts(wdf,1,3);
wdf=wdfilts(wdf,1,4);
wcf=wdsyn2s(wdf);
figure, imagesc((20*log10(abs(wcf))),[1 70]),colormap(gray);
Irf=ifwt2(wcf,w,escalas);

Irf=uint8(Irf);
figure,imshow(Irf);
imwrite(Irf,'result.png');
