function wd=wdsyn2s(wc)

c=wc{1,1}.sc+size(wc{1,1}.coeff,2)-1;
r=wc{1,2}.sr+size(wc{1,2}.coeff,1)-1;
I=zeros(r,c); %reconstructed image
wd=zeros(r,c); % wavelet decomposition
s=size(wc,1);%number of scales used for decomposition
for i=[1:s]
    if (i==s)
	sr=wc{i,4}.sr;	
	sc=wc{i,4}.sc;		
	dr=size(wc{i,4}.coeff,1);
	dc=size(wc{i,4}.coeff,2);
	wd(sr:sr+dr-1,sc:sc+dc-1)=wc{i,4}.coeff;
    endif
    for j=[1:3]
	sr=wc{i,j}.sr;	
	sc=wc{i,j}.sc;		
	dr=size(wc{i,j}.coeff,1);
	dc=size(wc{i,j}.coeff,2);
	wd(sr:sr+dr-1,sc:sc+dc-1)=wc{i,j}.coeff;
    endfor
endfor

endfunction


