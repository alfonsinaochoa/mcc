clear;
DENSITY = 0.05;

a = rgb2gray(imread('Lenna.jpg'));

%Salt and Pepper Noise
salt_pepper_noise = imnoise (a, 'salt & pepper', DENSITY);

%Plot
figure;
subplot(1,2,1), imshow(a);
title ('Imagen original en escala de grises');
subplot(1,2,2), imshow(salt_pepper_noise);
title ('Imagen con ruido sal y pimienta');


%se3 = strel('disk',3)
%figure, imshow(se3>0);
%ime = imerode(img,se3);
%figure, imshow(ime)
%imd = imdilate(img,se3);
%figure, imshow(imd)
%imo = imopen(img,se3);
%figure, imshow(imo)
%imc = imclose(img,se3);
%figure, imshow(imc)
