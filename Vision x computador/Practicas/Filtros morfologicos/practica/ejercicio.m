clear;
pkg load image
im = imread('sar.png');

#Filtro midpoint
F = imrest(im,'midpoint',3,3) ;
figure,imshow(F);

#Filtro morfologico
MN= [4;6];#tamaño para la figura
SE = strel('rectangle', MN);#definición de la figura

#prueba, dilatar y luego erosinar la imagen filtrada
IM3 = imdilate(F,SE);
IM2 = imerode(IM3,SE);
figure, imshow(IM2);

#prueba, dilatar y luego erosinar la imagen original
IM3 = imdilate(im,SE);
IM2 = imerode(IM3,SE);
figure, imshow(IM2);

#contar las estructuras en ambas imagenes
[x,y]=meshgrid(1:size(IM2,1), 1:size(IM2,2));
result=[x(:),y(:),IM2(:)]
%A=diag(randi(255,1,3));
%imref2d(size(A))
