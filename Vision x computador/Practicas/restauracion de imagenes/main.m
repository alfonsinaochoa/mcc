clear; close all;
h=2;
i=1;

imageFiles = dir('images/*');
arrayImages = java_array('java.lang.String', length(imageFiles) -3);
for image = h+1:length(imageFiles)
    fname = imageFiles(image).name;    
    arrayImages(image-h) = java.lang.String(fname);   
    OriginalImage = imread(fullfile('images/',imageFiles(image).name));
    HistEqImage = histeq(mat2gray(OriginalImage));
    %[height, width] = size(OriginalImage);
    %figure, imshow(mat2gray(OriginalImage));
    %figure, imhist(mat2gray(OriginalImage));
    figure(i),
    subplot(2,2,1);
    imshow(mat2gray(OriginalImage));
    subplot(2,2,2);
    imhist(mat2gray(OriginalImage))
    subplot(2,2,3);
    imshow(HistEqImage);
    subplot(2,2,4);
    imhist(HistEqImage);
    %figure, histeq(mat2gray(OriginalImage));
    title(imageFiles(image).name);
    %title(strcat('Histograma', imageFiles(image).name));
    hold    
    i = i + 1;
end
%I = im2double(imread('img/longitud470_24.tif'));
%figure, imshow(mat2gray(I));
%title('Original Image');
%figure(1), imhist(I)
%figure(2), imhist(mat2gray(I))

%J = wiener2(I);
%figure, imshow(mat2gray(J));
%title('Restored Image');



%put some motion blur
% LEN = 21;
% THETA = 11;
% PSF = fspecial('motion', LEN, THETA);
% blurred = imfilter(I, PSF, 'conv', 'circular');
% figure,imshow(blurred);
% title('Blurred Image');

%deblur
% wnr1 = deconvwnr(I, PSF, 0);
% figure, imshow(mat2gray(wnr1));
% title('Restored Image');
 
% %noise + blur
% noise_mean = 0;
% noise_var = 0.0001;
% blurred_noisy = imnoise(blurred, 'gaussian',noise_mean, noise_var);
% figure,imshow(blurred_noisy);
% title('Simulate Blur and Noise');
% 
% %intento 1
% wnr2 = deconvwnr(blurred_noisy, PSF, 0);
% figure,imshow(wnr2);
% title('Restoration of Blurred, Noisy Image Using NSR = 0');
% 
% 
% %intento 2
% signal_var = var(I(:));
% wnr3 = deconvwnr(blurred_noisy, PSF, noise_var / signal_var);
% figure,imshow(wnr3);
% title('Restoration of Blurred, Noisy Image Using Estimated NSR');
% 
% %quantizado
% blurred_quantized = imfilter(I, PSF, 'conv', 'circular');
% 
% % intento 1
% wnr4 = deconvwnr(blurred_quantized, PSF, 0);
% figure,imshow(wnr4);
% title('Restoration of blurred, quantized image using NSR = 0');
% 
% % intento 2
% uniform_quantization_var = (1/256)^2 / 12;
% signal_var = var(im2double(I(:)));
% wnr5 = deconvwnr(blurred_quantized, PSF, uniform_quantization_var / signal_var);
% figure,imshow(wnr5);
% title('Restoration of Blurred, Quantized Image Using Computed NSR');








