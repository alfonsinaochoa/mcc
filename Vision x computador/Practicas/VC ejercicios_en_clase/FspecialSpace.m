clear; close all;
HSIZE=9;
RADIUS=5;
SIGMA=1;
ALPHA=0.7;

im = imread('Lenna.jpg');
%imshow(im)
%improfile
img = rgb2gray(im);
%imshow(img)
%improfile
%h = fspecial('average',HSIZE);  
%h = fspecial('disk',RADIUS);
h = fspecial('gaussian',HSIZE,SIGMA);
%h = fspecial('laplacian',ALPHA);
%h = fspecial('log',HSIZE,SIGMA);
%h = fspecial('prewitt');
%h = fspecial('sobel');
%h = fspecial('unsharp',ALPHA);

%h = h'
figure,imshow(h);

figure,mesh(1:size(h,1),1:size(h,2),h);

bf = imfilter(im,h,'replicate','conv');

%figure,imshow(bf);

figure;
subplot(1,2,1), imshow(im);
subplot(1,2,2), imshow(bf);