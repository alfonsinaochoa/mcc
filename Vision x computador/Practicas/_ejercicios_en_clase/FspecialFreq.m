function []= FspecialFreq(im,type,param1,param2)
im=rgb2gray(im); 

switch type
case 'average'
h = fspecial(type,param1);
case 'sobel'
   h = fspecial(type);
case 'gaussian'
   h = fspecial(type,param2,param1);
case 'laplacian'
  h = fspecial(type,param1);

otherwise
   error('Unknown filter type.')
end

PQ = paddedsize(size(im));
F = fft2(double(im), PQ(1), PQ(2));
H = fft2(double(h), PQ(1), PQ(2));
FF_im = H.*F;
F_im = ifft2(FF_im);
F_im= F_im(2:size(im,1)+1, 2:size(im,2)+1);

%Display results (show all values)
figure, imshow(F_im,[])

end

