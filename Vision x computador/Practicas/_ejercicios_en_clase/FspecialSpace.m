HSIZE=9;
RADIUS=5;
SIGMA=1;
ALPHA=0.7;

% h = FSPECIAL('average',HSIZE);  
% h = FSPECIAL('disk',RADIUS);
% h = FSPECIAL('gaussian',HSIZE,SIGMA);
%  h = FSPECIAL('laplacian',ALPHA);
%  h = FSPECIAL('log',HSIZE,SIGMA);
%  h = FSPECIAL('prewitt');
%  h = FSPECIAL('sobel');
%  h = FSPECIAL('unsharp',ALPHA);

% h
% figure,imshow(h);

% figure,mesh(1:size(h,1),1:size(h,2),h);

bf = imfilter(im,h,'replicate','conv');

% figure,imshow(bf);

figure;
subplot(1,2,1), imshow(im);
subplot(1,2,2), imshow(bf);