function [] = filtertest(im)

%Convert to grayscale
im=rgb2gray(im); 
imshow(im)

%Determine good padding for Fourier transform
PQ = paddedsize(size(im));

%Create a Gaussian Lowpass filter 5% the width of the Fourier transform
D0 = 0.05*PQ(1);
H = lpfilter('ideal', PQ(1), PQ(2), D0);
fftshow(fftshift(H));

% Calculate the discrete Fourier transform of the image
F=fft2(double(im),size(H,1),size(H,2));

% Apply the filter to the Fourier spectrum of the image
FF_im = H.*F;

% convert the result to the spacial domain.
F_im=real(ifft2(FF_im)); 

% Crop the image to undo padding
F_im=F_im(1:size(im,1), 1:size(im,2));

%Display the blurred image
figure, imshow(F_im, [])

end